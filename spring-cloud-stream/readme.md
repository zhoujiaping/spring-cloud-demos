spring stream体系

spring cloud stream,spring cloud bus,spring messaging,spring integration

Spring Integration 
提供了 Spring 编程模型的扩展用来支持企业集成模式(Enterprise Integration Patterns)，
是对 Spring Messaging 的扩展。
可以实现快速封装企业流程，统一入出接口，可以简化大规模的业务系统中错综复杂的组件调用关系。
由于现阶段互联网微服务架构的流行，就象ESB企业服务总线一样，Spring Integration目前也处于比较尴尬的境地。

spring messaging
Spring Messaging 是 Spring Framework 中的一个模块，其作用就是统一消息的编程模型。

spring cloud stream
在 Spring Integration 的基础上进行了封装。
屏蔽了底层消息中间件的实现细节，希望以统一的一套 API 来进行消息的发送/消费，
底层消息中间件的实现细节由各消息中间件的 Binder 完成。

spring cloud bus
spring cloud bus在spring cloud stream的基础上再抽象了一层，可以用于事件广播，比如配置变更通知所有服务节点。
spring cloud bus能做的，spring cloud stream都能做，spring cloud bus为特定场景简化了使用方式。


参考文章

* 官网
* Spring Cloud Stream 体系及原理介绍 https://developer.aliyun.com/article/783841
* Spring Cloud Bus和Spring Cloud Stream的区别是啥？使用场景有啥区别？ https://www.zhihu.com/question/282619114

