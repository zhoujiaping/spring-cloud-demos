package com.example;

import org.example.support.EnableDispatchers;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDispatchers
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }

}
