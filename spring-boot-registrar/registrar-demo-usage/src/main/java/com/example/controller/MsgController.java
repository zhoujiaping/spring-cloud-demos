package com.example.controller;

import com.example.model.MsgSendReq;
import com.example.service.MsgSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/msg")
public class MsgController {
    @Autowired
    MsgSender msgSender;
    @GetMapping("/send")
    public Object sendMsg(MsgSendReq msgSendReq){
        return msgSender.send(msgSendReq);
    }
}
