package com.example.service.impl;

import com.example.model.MsgSendReq;
import com.example.service.MsgSender;
import org.example.dispatch.DispatcherTarget;

@DispatcherTarget(name ="mail",beanName = "mailMsgSender")
public class MailMsgSender implements MsgSender {
    @Override
    public String send(MsgSendReq msgSendReq) {
        return "send mail msg, msgSendReq is: "+msgSendReq;
    }
}
