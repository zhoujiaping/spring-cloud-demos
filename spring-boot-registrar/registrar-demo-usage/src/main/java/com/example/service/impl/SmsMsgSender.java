package com.example.service.impl;

import com.example.model.MsgSendReq;
import com.example.service.MsgSender;
import org.example.dispatch.DispatcherTarget;

@DispatcherTarget(name ="sms")
public class SmsMsgSender implements MsgSender {
    @Override
    public String send(MsgSendReq msgSendReq) {
        return "send sms msg, msgSendReq is: "+msgSendReq;
    }
}
