package com.example.service;

import com.example.model.MsgSendReq;
import org.example.dispatch.DispatcherMethod;
import org.example.support.Dispatcher;

@Dispatcher(name = "msg-sender")
public interface MsgSender {
    @DispatcherMethod(value = "#args[0].type")
    String send(MsgSendReq msgSendReq);
}
