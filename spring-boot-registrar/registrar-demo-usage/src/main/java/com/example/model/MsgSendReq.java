package com.example.model;

import lombok.Data;

@Data
public class MsgSendReq {
    String type;
    String content;
}
