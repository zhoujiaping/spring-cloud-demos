package org.example.support;

import lombok.SneakyThrows;
import org.example.dispatch.DispatcherFactory;
import org.springframework.beans.factory.FactoryBean;

/**
 * 分派委托对象工厂
 * @param <T> 需要被分派的接口
 */
public class DispatcherDelegateFactoryBean<T> implements FactoryBean<T> {
    /**
     * 需要被分派的接口
     */
    private Class<T> dispatcherClass;

    @SneakyThrows
    public DispatcherDelegateFactoryBean(String dispatcherClass){
        this.dispatcherClass = (Class<T>) Class.forName(dispatcherClass);
    }
    @Override
    public T getObject() {
        DispatcherFactory dispatcherFactory = DispatcherFactory.getInstance();
        return dispatcherFactory.newDispatcherDelegate(getObjectType());
    }

    @Override
    public Class<T> getObjectType() {
        return dispatcherClass;
    }
}
