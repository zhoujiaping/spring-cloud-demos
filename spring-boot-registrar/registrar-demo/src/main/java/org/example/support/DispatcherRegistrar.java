package org.example.support;

import lombok.SneakyThrows;
import org.example.dispatch.DispatcherFactory;
import org.example.dispatch.DispatcherTarget;
import org.example.dispatch.DispatcherProxy;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.*;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.function.Supplier;

/**
 * 动态bean注册
 * <p>
 * 参考
 * com.playtika.reactivefeign:feign-reactor-spring-configuration
 * reactivefeign.spring.config.ReactiveFeignClientsRegistrar
 */
public class DispatcherRegistrar implements ImportBeanDefinitionRegistrar,
        ResourceLoaderAware , EnvironmentAware {
    protected Environment environment;
    protected ResourceLoader resourceLoader;
    protected AnnotationBeanNameGenerator annotationBeanNameGenerator = new AnnotationBeanNameGenerator();

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
    //注册bean定义
    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
        //注册分派委托
        registerDispatcherDelegates(metadata, registry);
        //注册分派目标
        registerDispatcherTargets(metadata, registry);
    }

    @SneakyThrows
    private void registerDispatcherTargets(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
        //获取classpath的包扫描器
        ClassPathScanningCandidateComponentProvider scanner = getScanner();
        scanner.setResourceLoader(this.resourceLoader);
        Set<String> basePackages = getBasePackages(metadata);
        //注解类型过滤器，有指定注解的类被扫描到
        AnnotationTypeFilter dispatcherTargetAnnotationTypeFilter = new AnnotationTypeFilter(DispatcherTarget.class);
        scanner.addIncludeFilter(dispatcherTargetAnnotationTypeFilter);
        //遍历待扫描包的基本路径，获取候选组件集合
        for (String basePackage : basePackages) {
            Set<BeanDefinition> candidateComponents = scanner.findCandidateComponents(basePackage);
            for (BeanDefinition candidateComponent : candidateComponents) {
                //如果候选组件是通过注解定义的
                if (candidateComponent instanceof AnnotatedBeanDefinition) {
                    // verify annotated class is an interface
                    AnnotatedBeanDefinition beanDefinition = (AnnotatedBeanDefinition) candidateComponent;
                    AnnotationMetadata dispatcherMetadata = beanDefinition.getMetadata();
                    //获取注解属性
                    Map<String, Object> dispatcherTargetAttrs = dispatcherMetadata.getAnnotationAttributes(
                            DispatcherTarget.class.getCanonicalName());
                    Class dispatcherTargetClass = Class.forName(dispatcherMetadata.getClassName());
                    String dispatcherTargetBeanName = getBeanName(dispatcherTargetAttrs.get("beanName").toString(), beanDefinition, registry);
                    String dispatcherTargetName = dispatcherTargetAttrs.get("name").toString();
                    registerDispatcherTarget(dispatcherTargetBeanName, beanDefinition, registry);
                    for (Class<?> interfaceClazz : dispatcherTargetClass.getInterfaces()) {
                        Dispatcher dispatcher = interfaceClazz.getAnnotation(Dispatcher.class);
                        if (dispatcher != null) {
                            DispatcherProxy<?> dispatcherProxy = DispatcherFactory.getInstance().getDispatcherProxy(interfaceClazz);
                            Supplier<Object> dispatcherTargetSupplier = () ->
                                    ApplicationContextUtils.getAppCtx().getBean(dispatcherTargetBeanName, interfaceClazz);
                            dispatcherProxy.addDispatcherTargetObjectSupplier(dispatcherTargetName, dispatcherTargetSupplier);
                        }
                    }
                }
            }
        }
    }

    private String getBeanName(String beanName, AnnotatedBeanDefinition beanDefinition, BeanDefinitionRegistry registry) {
        if (StringUtils.hasText(beanName)) {
            return beanName;
        }
        return annotationBeanNameGenerator.generateBeanName(beanDefinition, registry);
    }

    @SneakyThrows
    private void registerDispatcherTarget(String dispatcherTargetBeanName, AnnotatedBeanDefinition beanDefinition, BeanDefinitionRegistry registry) {
        //设置beanClass
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(Class.forName(beanDefinition.getBeanClassName()));
        builder.setPrimary(false);
        //设置beanName
        registry.registerBeanDefinition(dispatcherTargetBeanName, builder.getBeanDefinition());
    }

    @SneakyThrows
    public void registerDispatcherDelegates(AnnotationMetadata metadata,
                                            BeanDefinitionRegistry registry) {
        //获取classpath的包扫描器
        ClassPathScanningCandidateComponentProvider scanner = getScanner();
        scanner.setResourceLoader(this.resourceLoader);
        Set<String> basePackages = getBasePackages(metadata);
        //注解类型过滤器，有指定注解的类被扫描到
        AnnotationTypeFilter dispatcherAnnotationTypeFilter = new AnnotationTypeFilter(
                Dispatcher.class);
        scanner.addIncludeFilter(dispatcherAnnotationTypeFilter);
        //遍历待扫描包的基本路径，获取候选组件集合
        for (String basePackage : basePackages) {
            Set<BeanDefinition> candidateComponents = scanner.findCandidateComponents(basePackage);
            for (BeanDefinition candidateComponent : candidateComponents) {
                //如果候选组件是通过注解定义的
                if (candidateComponent instanceof AnnotatedBeanDefinition) {
                    // verify annotated class is an interface
                    AnnotatedBeanDefinition beanDefinition = (AnnotatedBeanDefinition) candidateComponent;
                    AnnotationMetadata dispatcherMetadata = beanDefinition.getMetadata();
                    Assert.isTrue(dispatcherMetadata.isInterface(), "@Dispatcher can only be specified on an interface");
                    //获取注解属性
                    Map<String, Object> dispatcherAttrs = dispatcherMetadata.getAnnotationAttributes(Dispatcher.class.getCanonicalName());
                    String delegateBeanName = getBeanName(dispatcherAttrs.get("beanName").toString(), beanDefinition, registry);
                    registerDispatcherDelegateFactoryBean(delegateBeanName, registry, dispatcherMetadata, candidateComponent);
                    DispatcherFactory.getInstance().bindDispatcherProxy(Class.forName(dispatcherMetadata.getClassName()), new DispatcherProxy());
                }
            }
        }
    }

    private void registerDispatcherDelegateFactoryBean(String delegateBeanName, BeanDefinitionRegistry registry, AnnotationMetadata metadata, BeanDefinition candidateComponent) {
        //设置beanClass
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(DispatcherDelegateFactoryBean.class);
        builder.setPrimary(true);
        builder.addConstructorArgValue(metadata.getClassName());
        //设置beanName
        registry.registerBeanDefinition(delegateBeanName, builder.getBeanDefinition());
    }

    protected ClassPathScanningCandidateComponentProvider getScanner() {
        return new ClassPathScanningCandidateComponentProvider(false, this.environment) {
            //是否为候选组件
            @Override
            protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
                boolean isCandidate = false;
                //isCandidateComponent是否成立的条件是beanDefinitions对应的类是top class或者nested class且不是注解。
                if (beanDefinition.getMetadata().isIndependent()) {
                    if (!beanDefinition.getMetadata().isAnnotation()) {
                        isCandidate = true;
                    }
                }
                return isCandidate;
            }
        };
    }

    /**
     * 扫描的基本路径
     */
    protected Set<String> getBasePackages(AnnotationMetadata metadata) {
        Map<String, Object> attributes = metadata
                .getAnnotationAttributes(EnableDispatchers.class.getCanonicalName());

        Set<String> basePackages = new HashSet<>();
        for (String pkg : (String[]) attributes.get("value")) {
            if (StringUtils.hasText(pkg)) {
                basePackages.add(pkg);
            }
        }
        for (String pkg : (String[]) attributes.get("basePackages")) {
            if (StringUtils.hasText(pkg)) {
                basePackages.add(pkg);
            }
        }
        for (Class<?> clazz : (Class[]) attributes.get("basePackageClasses")) {
            basePackages.add(ClassUtils.getPackageName(clazz));
        }

        if (basePackages.isEmpty()) {
            basePackages.add(
                    ClassUtils.getPackageName(metadata.getClassName()));
        }
        return basePackages;
    }
}
