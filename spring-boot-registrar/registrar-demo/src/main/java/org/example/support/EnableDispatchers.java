package org.example.support;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({DispatcherRegistrar.class,DispatcherAutoConfiguration.class,ApplicationContextUtils.class})
public @interface EnableDispatchers {
    /**
     * 需要被扫描的包
     * @return
     */
    String[] value() default {};
    /**
     * 需要被扫描的包
     * @return
     */
    String[] basePackages() default {};
    /**
     * 需要被扫描的包
     * @return
     */
    Class[] basePackageClasses() default {};
}
