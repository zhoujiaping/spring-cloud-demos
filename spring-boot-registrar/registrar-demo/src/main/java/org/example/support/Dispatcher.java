package org.example.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 分派器
 * 接口被该注解标记，则说明该接口需要根据方法参数动态分派。
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Dispatcher {
    /**
     * 分派器名称
     * @return
     */
    String name();
    String beanName() default "";
}
