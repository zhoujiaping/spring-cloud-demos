package org.example.support;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * spring上下文工具类
 */
@Component
public class ApplicationContextUtils implements ApplicationContextAware {
    private static ApplicationContext appCtx;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext){
        ApplicationContextUtils.appCtx = applicationContext;
    }
    public static ApplicationContext getAppCtx(){
        return appCtx;
    }
}
