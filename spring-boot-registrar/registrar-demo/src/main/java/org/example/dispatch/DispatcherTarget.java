package org.example.dispatch;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 分派的目标
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DispatcherTarget {
    /**
     * 目标的名称
     * */
    String name();
    /**
     * 目标在spring上下文中的beanName
     * */
    String beanName() default "";
}
