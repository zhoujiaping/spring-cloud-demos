package org.example.dispatch;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * 分派器的代理
 * @param <T> 需要分派的接口
 */
public class DispatcherProxy<T> implements InvocationHandler {
    /**
     * 分派的目标集合。
     * 分派的目标的名称->分派的目标
     * 这里用Supplier是为了可以延迟获取分派的目标。
     */
    private Map<String, Supplier<T>> dispatchHandlerSuppliers = new HashMap<>();

    /**
     * 添加分派的目标
     * */
    public DispatcherProxy<T> addDispatcherTarget(T dispatchHandler) {
        String dispatcherTargetName;
        DispatcherTarget dispatcherTarget = dispatchHandler.getClass().getAnnotation(DispatcherTarget.class);
        if (dispatcherTarget == null) {
            throw new RuntimeException("dispatcherTarget must implements DispatchHandler interface or has @DispatcherTarget annotation!");
        }
        dispatcherTargetName = dispatcherTarget.name();
        dispatchHandlerSuppliers.put(dispatcherTargetName, ()->dispatchHandler);
        return this;
    }

    /**
     * 添加分派的目标
     * @param dispatcherTargetName
     * @param dispatchHandlerSupplier
     * @return
     */
    public DispatcherProxy<T> addDispatcherTargetObjectSupplier(String dispatcherTargetName, Supplier<Object> dispatchHandlerSupplier) {
        dispatchHandlerSuppliers.put(dispatcherTargetName, (Supplier<T>) dispatchHandlerSupplier);
        return this;
    }
    /**
     * 添加分派的目标
     * @param dispatcherTargetName
     * @param dispatchHandlerSupplier
     * @return
     */
    public DispatcherProxy<T> addDispatcherTargetSupplier(String dispatcherTargetName, Supplier<T> dispatchHandlerSupplier) {
        dispatchHandlerSuppliers.put(dispatcherTargetName, dispatchHandlerSupplier);
        return this;
    }

    /**
     * 动态代理的拦截方法
     * @param proxy
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        Class<?>[] parameterTypes = method.getParameterTypes();

        /*
           查看生成代理的源码，比如jdk17。java.lang.reflect.ProxyGenerator.generateClassFile
           addProxyMethod(hashCodeMethod);
           addProxyMethod(equalsMethod);
           addProxyMethod(toStringMethod);

            for (Class<?> intf : interfaces) {
                for (Method m : intf.getMethods()) {
                    if (!Modifier.isStatic(m.getModifiers())) {
                        addProxyMethod(m, intf);
                    }
                }
            }
            动态代理会代理接口中定义的方法，外加hashCode、equals、toString三个方法。
        */
        // 如果 toString、hashCode 和 equals 等方法被子类重写了，这里也直接调用
        if ("toString".equals(methodName) && parameterTypes.length == 0) {
            return proxy.getClass().getName() + "@" + System.identityHashCode(proxy);
        }
        if ("hashCode".equals(methodName) && parameterTypes.length == 0) {
            return System.identityHashCode(this);
        }
        if ("equals".equals(methodName) && parameterTypes.length == 1) {
            return proxy == args[0];
        }
        //如果方法需要被分派，则会有@DispatcherMethod注解
        DispatcherMethod dispatcherMethod = method.getAnnotation(DispatcherMethod.class);
        if (dispatcherMethod == null) {
            throw new RuntimeException("@DispatcherMethod not found on method "+ methodName);
        }
        //获取spel表达式，计算目标对象的名称
        String expression = dispatcherMethod.value();
        String dispatchHandlerName = parseExpression(expression, args);
        //获取目标对象
        Object dispatchHandler = dispatchHandlerSuppliers.get(dispatchHandlerName).get();
        //分派给目标对象执行
        return method.invoke(dispatchHandler, args);
    }
    public String parseExpression(String expression, Object args) {
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("args", args);
        Expression exp = parser.parseExpression(expression);
        //取出解析结果
        String result = exp.getValue(context).toString();
        return result;
    }
}
