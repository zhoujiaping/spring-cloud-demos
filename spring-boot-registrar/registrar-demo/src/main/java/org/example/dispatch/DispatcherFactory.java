package org.example.dispatch;

import org.springframework.util.Assert;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * 分派器工厂
 */
public class DispatcherFactory {

    private static final DispatcherFactory dispatcherFactory = new DispatcherFactory();

    /**
     * 接口->分派器代理
     * 接口根据参数分派到某个实现类，需要一个代理。
     */
    private Map<Class<?>, DispatcherProxy> dispatcherProxies = new HashMap<>();

    private DispatcherFactory() {
    }

    public static DispatcherFactory getInstance() {
        return dispatcherFactory;
    }

    /**
     * 绑定接口和代理对象（因为代理是有状态的，代理对象保存了一组目标对象。）
     *
     * @param clazz
     * @param handler
     * @return
     */
    public DispatcherFactory bindDispatcherProxy(Class<?> clazz, DispatcherProxy handler) {
        dispatcherProxies.put(clazz, handler);
        return this;
    }

    /**
     * 获取接口绑定的代理对象
     */
    public DispatcherProxy getDispatcherProxy(Class<?> clazz) {
        return dispatcherProxies.get(clazz);
    }

    /**
     * 创建接口的委托对象（委托对象是jdk动态代理生成的）。
     * 委托 vs 代理？
     * 这里的委托对象是jdk动态代理生成的。jdk动态代理需要一个invocationHandler，这个invocationHandler就是上面说的代理。
     */
    public <T> T newDispatcherDelegate(Class<T> clazz) {
        DispatcherProxy<T> proxy = dispatcherProxies.get(clazz);
        Assert.notNull(proxy, "DispatcherProxy not found for " + clazz);
        return newDispatcherDelegate(clazz, proxy);
    }

    /**
     * 创建接口的委托对象（委托对象是jdk动态代理生成的）。
     */
    public <T> T newDispatcherDelegate(Class<T> clazz, DispatcherProxy<T> invocationHandler) {
        return (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, invocationHandler);
    }
}
