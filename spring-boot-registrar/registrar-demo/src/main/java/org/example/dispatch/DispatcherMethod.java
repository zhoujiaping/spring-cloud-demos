package org.example.dispatch;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * 需要分派的方法
 * */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DispatcherMethod {
    /**
     * 获取分派目标名称的spring expression language（spel）。
     * 根据spel和方法入参求值得到分派目标的名称
     * */
    String value();
    /**
     * 拓展，支持不同的 表达式求值结果和分派目标名称 匹配规则。
     * 比如通过正则匹配，将方法执行分派到所有匹配结果的目标。
     * */

}
