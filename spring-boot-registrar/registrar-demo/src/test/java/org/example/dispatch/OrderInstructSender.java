package org.example.dispatch;

import java.util.Map;

@DispatcherTarget(name ="order")
public class OrderInstructSender implements InstructSender{
    @Override
    public Object send(String url, Map<String, Object> data) {
        return this.getClass().getSimpleName()+":send instruct ,url= "+url+", data= "+data;
    }
}
