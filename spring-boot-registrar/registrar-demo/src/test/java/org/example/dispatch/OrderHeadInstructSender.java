package org.example.dispatch;

import java.util.Map;

@DispatcherTarget(name ="order-head")
public class OrderHeadInstructSender implements InstructSender{
    @Override
    public Object send(String url, Map<String, Object> data) {
        return this.getClass().getSimpleName()+":send instruct ,url= "+url+", data= "+data;
    }
}
