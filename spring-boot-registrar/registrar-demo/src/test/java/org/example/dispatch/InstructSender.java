package org.example.dispatch;

import java.util.Map;

public interface InstructSender{
    @DispatcherMethod(value = "#args[1].get('type')")
    Object send(String url, Map<String,Object> data);
}
