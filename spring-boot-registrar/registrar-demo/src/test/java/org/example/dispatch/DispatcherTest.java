package org.example.dispatch;


import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Map;
import java.util.TreeMap;

public class DispatcherTest {
    @SneakyThrows
    @Test
    public void testDispatch(){
        DispatcherFactory dispatcherFactory = DispatcherFactory.getInstance();
        DispatcherProxy proxy = new DispatcherProxy<>();
        InstructSender sender = dispatcherFactory.newDispatcherDelegate(InstructSender.class,proxy);
        System.out.println("sender=>"+sender);

        proxy.addDispatcherTarget(new OrderInstructSender());
        proxy.addDispatcherTarget(new OrderHeadInstructSender());

        Object res1 = sender.send("http://localhost:8080", new TreeMap(Map.of("hello","world","type","order")));
        System.out.println("res1=>"+res1);
        Assert.assertEquals("OrderInstructSender:send instruct ,url= http://localhost:8080, data= {hello=world, type=order}",res1);


        Object res2 = sender.send("http://localhost:8080", new TreeMap(Map.of("hello","world","type","order-head")));
        System.out.println("res2=>"+res2);
        Assert.assertEquals("OrderHeadInstructSender:send instruct ,url= http://localhost:8080, data= {hello=world, type=order-head}",res2);

    }
    @Test
    public void testParseExpression(){
        String res = parseExpression("#args[0]",new Object[]{"http://localhost:8080", Map.of("hello","world","type","order")});
        System.out.println(res);
        Assert.assertEquals("http://localhost:8080",res);
    }
    public String parseExpression(String expression, Object args) {
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("args", args);
        Expression exp = parser.parseExpression(expression);
        //取出解析结果
        String result = exp.getValue(context).toString();
        return result;
    }

}
