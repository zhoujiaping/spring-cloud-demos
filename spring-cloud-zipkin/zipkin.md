Zipkin 是一个开放源代码分布式的跟踪系统，由Twitter公司开源，它致力于收集服务的定时数据，以解决微服务架构中的延迟问题，包括数据的收集、存储、查找和展现。

每个服务向zipkin报告计时数据，zipkin会根据调用关系通过Zipkin UI生成依赖关系图，显示了多少跟踪请求通过每个服务，该系统让开发者可通过一个 Web 前端轻松的收集和分析数据，例如用户每次请求服务的处理时间等，可方便的监测系统中存在的瓶颈。

Zipkin提供了可插拔数据存储方式：In-Memory、MySql、Cassandra以及Elasticsearch。接下来的测试为方便直接采用In-Memory方式进行存储，生产推荐Elasticsearch。

官网
https://zipkin.io/

按照官网上安装zipkin
# get the latest source
git clone https://github.com/openzipkin/zipkin
cd zipkin
# Build the server and also make its dependencies
./mvnw -DskipTests --also-make -pl zipkin-server clean install

修改zipkin-server\src\main\resources\zipkin-server-shared.yml

配置mysql连接

将存储方式改为mysql（zipkin.storage.type=mysql）

# Run the server
java -jar ./zipkin-server/target/zipkin-server-*exec.jar

或者直接下载jar包，添加启动参数 --zipkin.storage.type=mysql --zipkin.storage.mysql.jdbc-url=jdbc:mysql://localhost:3306/zipkin
--zipkin.storage.mysql.username=root --zipkin.storage.mysql.password=123456

初始化数据库
zipkin-storage\mysql-v1\src\main\resources\mysql.sql

（使用mysql是因为本地正好安装了mysql）

访问页面查看日志 http://localhost:9411/zipkin/

访问微服务生成日志 http://localhost:8080/echo
