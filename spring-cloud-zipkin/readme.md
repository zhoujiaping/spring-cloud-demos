官网地址 

https://docs.spring.io/spring-cloud-sleuth/docs/3.1.1-SNAPSHOT/reference/htmlsingle/spring-cloud-sleuth.html

上面的图片加载不了，不方便理解，还好官网提供了一个pdf文档

https://docs.spring.io/spring-cloud-sleuth/docs/3.1.1-SNAPSHOT/reference/pdf/spring-cloud-sleuth.pdf


日志样例
```
2022-01-11 14:06:07.059  INFO [example-server,7fbab2ae1d3357f8,4098e7f473daa62b] 24848 --- [ctor-http-nio-2] org.example.controller.EchoController    : before echo
```
日志格式
```
[application name,trace id,span id]
```

场景
* http请求
* 消息队列事件
* 定时任务批处理
* 异步/新线程
* 非http协议的rpc调用


example

http://localhost:8080/echo