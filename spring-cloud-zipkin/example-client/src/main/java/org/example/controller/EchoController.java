package org.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.IntStream;

@RestController
@RequestMapping
@Slf4j
public class EchoController {
    @Autowired
    WebClient webClient;
    @GetMapping("/echo")
    public Mono<String> echo(){
        log.info("before echo");
        return webClient.get().uri("http://localhost:8081/echo").retrieve().bodyToMono(String.class);
    }

    /*
     * 自定义span。
     * 场景：定时任务，逐条处理数据，每条数据单独追踪。
     */
    @Autowired
    Tracer tracer;
    @GetMapping("/task")
    public Mono<String> task(){
        log.info("before echo");
        IntStream.range(1,10).forEachOrdered(i->{
            Span newSpan = tracer.nextSpan().name("xxx-task");
            try(Tracer.SpanInScope ws = tracer.withSpan(newSpan.start())) {
                log.info("task item {}", i);
            }finally {
                newSpan.end();
            }
        });
        return Mono.just("ok");
    }
}
