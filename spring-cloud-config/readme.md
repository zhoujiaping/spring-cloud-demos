# 关于boostrap.properties/bootstrap.yml
spring cloud加载配置流程
加载外部配置->启动应用->加载应用配置
所以需要一个在启动应用之前加载外部配置的功能，
这个功能由spring-cloud-context/spring-cloud-starter-bootstrap提供。

# Spring Cloud 2020 bootstrap 配置文件失效
https://cloud.tencent.com/developer/article/1786283

spring boot 2.3.1.RELEASE
依赖org.springframework.cloud:spring-cloud-context
在 Spring Boot 中有两种上下文，一种是 bootstrap, 
另外一种是 application, bootstrap 是应用程序的父上下文，
也就是说 bootstrap 加载优先于 applicaton。
bootstrap 主要用于从额外的资源来加载配置信息，还可以在本地外部配置文件中解密属性。
这两个上下文共用一个环境，它是任何Spring应用程序的外部属性的来源。
bootstrap 里面的属性会优先加载，它们默认也不能被本地相同配置覆盖。

Spring Cloud 2020
依赖org.springframework.cloud:spring-cloud-starter-bootstrap

# nacos中的概念（官网https://nacos.io/zh-cn/docs/concepts.html）
就像Maven用groupId、artifactId、version三者来定位jar包在仓库中的位置一样，
Nacos也提供了 Namespace (命名空间) 、Data ID (配置集ID)、 Group (组) 来确定一个配置文件（或者叫配置集）

namespace可以用于环境/租户隔离，每个namespace下面的配置完全隔离。
group可以用于为配置集进行分组（分为公共组和业务组，公共组的配置所有微服务共享，比如一些中间件的配置）。
dataId用来确定一个配置集，一般对应一个服务名。

# 配置分组方案示例
有两个微服务，分别为订单微服务（order-server）和会员微服务（member-server）。
两个微服务都需要访问同一个redis。

配置分组方案(逻辑结构)
namespace=dev
    group=shared-group
        data-id=redis-server
            redis.host=10.118.238.9
            redis.port=6379
    group=biz-group
        data-id=order-server.properties
            db.host=10.118.238.10
            db.connect.timeout=1000
    group=biz-group
        data-id=member-server.properties
            db.host=10.118.238.11
            db.port=3306
然后测试环境、生产环境分别复制一份以上配置。

参考 
聊聊Nacos配置隔离和分类的使用
https://cloud.tencent.com/developer/article/1545157

namespace=dev和spring.profiles.active=dev比较
相同点：都可以实现多环境配置
不同点：nacos支持对namespace的权限控制
二者可以配合使用，比如开发/测试环境有多套，不同的开发/测试环境，可以通过spring.profiles.active指定。

# 配置的优先级
共享配置 < 扩展配置 < 服务名 < 服务名.文件扩展名 < 服务名-环境.文件扩展名


# 实战演练
1. 启动nacos服务（startup.cmd -m standalone）
2. 初始化命名空间，配置（执行test/resources/init-config.groovy）
3. 启动org.example.App
4. 访问http://localhost:8080/config/echo和http://localhost:8080/config/redis，查看配置
5. 访问http://localhost:8848/nacos/index.html，修改配置
6. 访问http://localhost:8080/config/echo和http://localhost:8080/config/redis，查看配置是否被刷新




