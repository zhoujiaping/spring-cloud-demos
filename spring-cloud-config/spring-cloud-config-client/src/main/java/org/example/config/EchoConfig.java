package org.example.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

//@RefreshScope
@Data
@ConfigurationProperties(prefix = "echo")
@Component
public class EchoConfig {
    String msg;
}
