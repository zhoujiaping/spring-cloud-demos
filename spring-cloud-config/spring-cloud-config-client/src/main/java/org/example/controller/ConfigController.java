package org.example.controller;

import org.example.config.EchoConfig;
import org.example.config.RedisConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

//通过 Spring Cloud 原生注解 @RefreshScope 实现配置自动更新
@RestController
@RequestMapping("/config")
public class ConfigController {
    @Autowired
    EchoConfig echoConfig;
    @Autowired
    RedisConfig redisConfig;

    @GetMapping("/echo")
    public Mono<String> echo() {
        return Mono.just(echoConfig.getMsg());
    }
    @GetMapping("/redis")
    public Mono<String> redis() {
        return Mono.just(redisConfig.getHost()+":"+redisConfig.getPort());
    }
}
