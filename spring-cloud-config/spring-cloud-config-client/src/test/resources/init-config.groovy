import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.RequestEntity
import org.springframework.web.client.RestTemplate

//https://nacos.io/zh-cn/docs/open-api.html
//初始化
def rest = new RestTemplate()
def contextUrl = "http://127.0.0.1:8848"
//查询命名空间列表
def nsRes = rest.getForObject("$contextUrl/nacos/v1/console/namespaces",Map)
println "查询命名空间列表<==$nsRes"
if(nsRes.code!=200){
    throw new RuntimeException("查询命名空间列表异常")
}
def nsList = nsRes.data
def nsId = 'a6e4d016-dcfc-472f-9679-d6b1a8296544'
def devNs = nsList.find{
    it.namespaceShowName == 'dev'
}
if(!devNs){
    //创建命名空间
    def params = "customNamespaceId=$nsId&namespaceName=dev"
    def createNsRes = rest.postForObject("$contextUrl/nacos/v1/console/namespaces?$params",null,Map)
    println "创建命名空间<==$createNsRes"
}else{
    nsId = devNs.namespace
}

//创建组（自动创建）

//发布配置
//curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=spring-cloud-config-client-dev.properties&group=test&content=HelloWorld"

def publishSharedConfigReqBody = [
        tenant:nsId,
        dataId:'redis-server',
        group:'shared-group',
        content:"redis.host=localhost\nredis.port=6379"
].collect{
    "${it.key}=${it.value}"
}.join("&")
def publishSharedConfigReqHeaders = new HttpHeaders()
publishSharedConfigReqHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED)
def publishSharedConfigReq = new RequestEntity(publishSharedConfigReqBody,publishSharedConfigReqHeaders, HttpMethod.POST,new URI("$contextUrl/nacos/v1/cs/configs"))
def publishSharedConfigRes = rest.exchange(publishSharedConfigReq,Boolean)
println "发布公共配置<==$publishSharedConfigRes"


def publishBizConfigReqBody = [
        tenant:nsId,
        dataId:'spring-cloud-config-client',
        group:'biz-group',
        content:"echo.msg=helloworld"
].collect{
    "${it.key}=${it.value}"
}.join("&")
def publishBizConfigReqHeaders = new HttpHeaders()
publishBizConfigReqHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED)
def publishBizConfigReq = new RequestEntity(publishBizConfigReqBody,publishBizConfigReqHeaders, HttpMethod.POST,new URI("$contextUrl/nacos/v1/cs/configs"))
def publishBizConfigRes = rest.exchange(publishBizConfigReq,Boolean)
println "发布业务配置<==$publishBizConfigRes"


//获取配置 curl -X GET "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=spring-cloud-config-client-dev.properties&group=test"