[INFO] Scanning for projects...
[INFO] 
[INFO] ---------------< org.example:spring-cloud-gateway-8080 >----------------
[INFO] Building spring-cloud-gateway-8080 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-dependency-plugin:2.8:tree (default-cli) @ spring-cloud-gateway-8080 ---
[INFO] org.example:spring-cloud-gateway-8080:jar:1.0-SNAPSHOT
[INFO] +- org.springframework.cloud:spring-cloud-starter-gateway:jar:3.0.4:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-starter:jar:3.0.4:compile
[INFO] |  |  +- org.springframework.cloud:spring-cloud-context:jar:3.0.4:compile
[INFO] |  |  |  \- org.springframework.security:spring-security-crypto:jar:5.4.9:compile
[INFO] |  |  +- org.springframework.cloud:spring-cloud-commons:jar:3.0.4:compile
[INFO] |  |  \- org.springframework.security:spring-security-rsa:jar:1.0.10.RELEASE:compile
[INFO] |  |     \- org.bouncycastle:bcpkix-jdk15on:jar:1.68:compile
[INFO] |  |        \- org.bouncycastle:bcprov-jdk15on:jar:1.68:compile
[INFO] |  \- org.springframework.cloud:spring-cloud-gateway-server:jar:3.0.4:compile
[INFO] |     +- org.springframework.boot:spring-boot-starter-validation:jar:2.4.13:compile
[INFO] |     |  +- org.glassfish:jakarta.el:jar:3.0.4:compile
[INFO] |     |  \- org.hibernate.validator:hibernate-validator:jar:6.1.7.Final:compile
[INFO] |     |     +- jakarta.validation:jakarta.validation-api:jar:2.0.2:compile
[INFO] |     |     +- org.jboss.logging:jboss-logging:jar:3.4.2.Final:compile
[INFO] |     |     \- com.fasterxml:classmate:jar:1.5.1:compile
[INFO] |     \- io.projectreactor.addons:reactor-extra:jar:3.4.5:compile
[INFO] +- org.springframework.cloud:spring-cloud-starter-circuitbreaker-reactor-resilience4j:jar:2.0.2:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-circuitbreaker-resilience4j:jar:2.0.2:compile
[INFO] |  |  \- io.github.resilience4j:resilience4j-spring-boot2:jar:1.7.0:compile
[INFO] |  |     +- io.github.resilience4j:resilience4j-spring:jar:1.7.0:compile
[INFO] |  |     |  +- io.github.resilience4j:resilience4j-annotations:jar:1.7.0:compile
[INFO] |  |     |  +- io.github.resilience4j:resilience4j-consumer:jar:1.7.0:compile
[INFO] |  |     |  |  \- io.github.resilience4j:resilience4j-circularbuffer:jar:1.7.0:compile
[INFO] |  |     |  \- io.github.resilience4j:resilience4j-framework-common:jar:1.7.0:compile
[INFO] |  |     |     +- io.github.resilience4j:resilience4j-ratelimiter:jar:1.7.0:compile
[INFO] |  |     |     \- io.github.resilience4j:resilience4j-retry:jar:1.7.0:compile
[INFO] |  |     \- io.github.resilience4j:resilience4j-micrometer:jar:1.7.0:compile
[INFO] |  +- io.github.resilience4j:resilience4j-circuitbreaker:jar:1.7.0:compile
[INFO] |  |  +- io.vavr:vavr:jar:0.10.2:compile
[INFO] |  |  |  \- io.vavr:vavr-match:jar:0.10.2:compile
[INFO] |  |  +- org.slf4j:slf4j-api:jar:1.7.32:compile
[INFO] |  |  \- io.github.resilience4j:resilience4j-core:jar:1.7.0:compile
[INFO] |  +- io.github.resilience4j:resilience4j-timelimiter:jar:1.7.0:compile
[INFO] |  \- io.github.resilience4j:resilience4j-reactor:jar:1.7.0:compile
[INFO] +- com.squareup.okhttp3:okhttp:jar:4.9.3:compile
[INFO] |  +- com.squareup.okio:okio:jar:2.8.0:compile
[INFO] |  |  \- org.jetbrains.kotlin:kotlin-stdlib-common:jar:1.4.32:compile
[INFO] |  \- org.jetbrains.kotlin:kotlin-stdlib:jar:1.4.32:compile
[INFO] |     \- org.jetbrains:annotations:jar:13.0:compile
[INFO] +- org.junit.jupiter:junit-jupiter-api:jar:5.8.2:test
[INFO] |  +- org.opentest4j:opentest4j:jar:1.2.0:compile
[INFO] |  +- org.junit.platform:junit-platform-commons:jar:1.7.2:compile
[INFO] |  \- org.apiguardian:apiguardian-api:jar:1.1.2:compile
[INFO] +- org.junit.jupiter:junit-jupiter-engine:jar:5.8.2:test
[INFO] |  \- org.junit.platform:junit-platform-engine:jar:1.7.2:compile
[INFO] +- org.springframework.boot:spring-boot-starter:jar:2.4.13:compile
[INFO] |  +- org.springframework.boot:spring-boot:jar:2.4.13:compile
[INFO] |  |  \- org.springframework:spring-context:jar:5.3.13:compile
[INFO] |  |     +- org.springframework:spring-aop:jar:5.3.13:compile
[INFO] |  |     \- org.springframework:spring-expression:jar:5.3.13:compile
[INFO] |  +- org.springframework.boot:spring-boot-autoconfigure:jar:2.4.13:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-logging:jar:2.4.13:compile
[INFO] |  |  +- ch.qos.logback:logback-classic:jar:1.2.7:compile
[INFO] |  |  |  \- ch.qos.logback:logback-core:jar:1.2.7:compile
[INFO] |  |  +- org.apache.logging.log4j:log4j-to-slf4j:jar:2.13.3:compile
[INFO] |  |  |  \- org.apache.logging.log4j:log4j-api:jar:2.13.3:compile
[INFO] |  |  \- org.slf4j:jul-to-slf4j:jar:1.7.32:compile
[INFO] |  +- jakarta.annotation:jakarta.annotation-api:jar:1.3.5:compile
[INFO] |  +- org.springframework:spring-core:jar:5.3.13:compile
[INFO] |  |  \- org.springframework:spring-jcl:jar:5.3.13:compile
[INFO] |  \- org.yaml:snakeyaml:jar:1.27:compile
[INFO] +- org.springframework.boot:spring-boot-starter-webflux:jar:2.4.13:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-json:jar:2.4.13:compile
[INFO] |  |  +- com.fasterxml.jackson.core:jackson-databind:jar:2.11.4:compile
[INFO] |  |  |  +- com.fasterxml.jackson.core:jackson-annotations:jar:2.11.4:compile
[INFO] |  |  |  \- com.fasterxml.jackson.core:jackson-core:jar:2.11.4:compile
[INFO] |  |  +- com.fasterxml.jackson.datatype:jackson-datatype-jdk8:jar:2.11.4:compile
[INFO] |  |  +- com.fasterxml.jackson.datatype:jackson-datatype-jsr310:jar:2.11.4:compile
[INFO] |  |  \- com.fasterxml.jackson.module:jackson-module-parameter-names:jar:2.11.4:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-reactor-netty:jar:2.4.13:compile
[INFO] |  |  \- io.projectreactor.netty:reactor-netty-http:jar:1.0.13:compile
[INFO] |  |     +- io.netty:netty-codec-http:jar:4.1.70.Final:compile
[INFO] |  |     |  +- io.netty:netty-common:jar:4.1.70.Final:compile
[INFO] |  |     |  +- io.netty:netty-buffer:jar:4.1.70.Final:compile
[INFO] |  |     |  +- io.netty:netty-transport:jar:4.1.70.Final:compile
[INFO] |  |     |  +- io.netty:netty-codec:jar:4.1.70.Final:compile
[INFO] |  |     |  \- io.netty:netty-handler:jar:4.1.70.Final:compile
[INFO] |  |     +- io.netty:netty-codec-http2:jar:4.1.70.Final:compile
[INFO] |  |     +- io.netty:netty-resolver-dns:jar:4.1.70.Final:compile
[INFO] |  |     |  +- io.netty:netty-resolver:jar:4.1.70.Final:compile
[INFO] |  |     |  \- io.netty:netty-codec-dns:jar:4.1.70.Final:compile
[INFO] |  |     +- io.netty:netty-resolver-dns-native-macos:jar:osx-x86_64:4.1.70.Final:compile
[INFO] |  |     |  \- io.netty:netty-resolver-dns-classes-macos:jar:4.1.70.Final:compile
[INFO] |  |     +- io.netty:netty-transport-native-epoll:jar:linux-x86_64:4.1.70.Final:compile
[INFO] |  |     |  +- io.netty:netty-transport-native-unix-common:jar:4.1.70.Final:compile
[INFO] |  |     |  \- io.netty:netty-transport-classes-epoll:jar:4.1.70.Final:compile
[INFO] |  |     \- io.projectreactor.netty:reactor-netty-core:jar:1.0.13:compile
[INFO] |  |        \- io.netty:netty-handler-proxy:jar:4.1.70.Final:compile
[INFO] |  |           \- io.netty:netty-codec-socks:jar:4.1.70.Final:compile
[INFO] |  +- org.springframework:spring-web:jar:5.3.13:compile
[INFO] |  |  \- org.springframework:spring-beans:jar:5.3.13:compile
[INFO] |  \- org.springframework:spring-webflux:jar:5.3.13:compile
[INFO] |     \- io.projectreactor:reactor-core:jar:3.4.12:compile
[INFO] |        \- org.reactivestreams:reactive-streams:jar:1.0.3:compile
[INFO] +- org.projectlombok:lombok:jar:1.18.22:provided
[INFO] +- org.springframework.cloud:spring-cloud-starter-bootstrap:jar:3.0.4:compile
[INFO] \- org.codehaus.groovy:groovy-all:pom:3.0.9:compile
[INFO]    +- org.codehaus.groovy:groovy:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-ant:jar:2.5.15:compile
[INFO]    |  +- org.apache.ant:ant:jar:1.9.15:compile
[INFO]    |  +- org.apache.ant:ant-junit:jar:1.9.15:runtime
[INFO]    |  +- org.apache.ant:ant-launcher:jar:1.9.15:compile
[INFO]    |  \- org.apache.ant:ant-antlr:jar:1.9.15:runtime
[INFO]    +- org.codehaus.groovy:groovy-astbuilder:jar:3.0.9:compile
[INFO]    +- org.codehaus.groovy:groovy-cli-picocli:jar:2.5.15:compile
[INFO]    |  \- info.picocli:picocli:jar:4.3.2:compile
[INFO]    +- org.codehaus.groovy:groovy-console:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-datetime:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-docgenerator:jar:2.5.15:compile
[INFO]    |  \- com.thoughtworks.qdox:qdox:jar:1.12.1:compile
[INFO]    +- org.codehaus.groovy:groovy-groovydoc:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-groovysh:jar:2.5.15:compile
[INFO]    |  \- jline:jline:jar:2.14.6:compile
[INFO]    +- org.codehaus.groovy:groovy-jmx:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-json:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-jsr223:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-macro:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-nio:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-servlet:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-sql:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-swing:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-templates:jar:2.5.15:compile
[INFO]    +- org.codehaus.groovy:groovy-test:jar:2.5.15:compile
[INFO]    |  \- junit:junit:jar:4.13.2:compile
[INFO]    |     \- org.hamcrest:hamcrest-core:jar:2.2:compile
[INFO]    |        \- org.hamcrest:hamcrest:jar:2.2:compile
[INFO]    +- org.codehaus.groovy:groovy-test-junit5:jar:2.5.15:compile
[INFO]    |  \- org.junit.platform:junit-platform-launcher:jar:1.7.2:compile
[INFO]    +- org.codehaus.groovy:groovy-testng:jar:2.5.15:compile
[INFO]    |  \- org.testng:testng:jar:6.13.1:runtime
[INFO]    |     \- com.beust:jcommander:jar:1.72:runtime
[INFO]    \- org.codehaus.groovy:groovy-xml:jar:2.5.15:compile
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  3.584 s
[INFO] Finished at: 2022-01-02T04:24:44+08:00
[INFO] ------------------------------------------------------------------------
