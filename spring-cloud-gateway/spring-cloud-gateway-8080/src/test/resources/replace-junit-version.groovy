def root = new File('E:/IdeaProjects/spring-cloud-demos')

root.eachFileRecurse {
    if(it.name != "pom.xml"){
        return
    }
    def text = it.text
    def sb = new StringBuilder()
    def idx = text.indexOf("<artifactId>junit-jupiter-api</artifactId>")
    if (idx > 0) {
        idx = text.indexOf("<version>5.8.2</version>", idx)
        if(idx>0){
            sb.append(text, 0, idx)
                    .append("<version>5.7.0</version>")
                    .append(text,idx+"<version>5.8.2</version>".length(),text.length())
            it.text = sb.toString()
            println "replaced file $it"
        }
    }
}
root.eachFileRecurse{
    if(it.name != "pom.xml"){
        return
    }
    def text = it.text
    def sb = new StringBuilder()
    def idx = text.indexOf("<artifactId>junit-jupiter-engine</artifactId>")
    if (idx > 0) {
        idx = text.indexOf("<version>5.8.2</version>", idx)
        if(idx>0){
            sb.append(text, 0, idx)
                    .append("<version>5.7.0</version>")
                    .append(text,idx+"<version>5.8.2</version>".length(),text.length())
            it.text = sb.toString()
            println "replaced file $it"
        }
    }
}

