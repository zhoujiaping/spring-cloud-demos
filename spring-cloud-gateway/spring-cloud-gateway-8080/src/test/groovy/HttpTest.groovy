import lombok.Cleanup
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.junit.jupiter.api.Test

class HttpTest {
    OkHttpClient client = new OkHttpClient();

    @Test
    void testHelloWorld(){
        String url = "https://www.baidu.com"
        Request request = new Request.Builder()
                .url(url)
                .build()

        @Cleanup Response response = client.newCall(request).execute()
        println response.body().string()
    }
    @Test
    void testRouteMembers(){
        String url = "http://localhost:8080/members"
        Request request = new Request.Builder()
                .url(url)
                .build()

        @Cleanup Response response = client.newCall(request).execute()
        def content = response.body().string()
        assert content.contains("captain jack sparrow")
    }
    @Test
    void testRouteOrders(){
        String url = "http://localhost:8080/orders"
        Request request = new Request.Builder()
                .url(url)
                .build()

        @Cleanup Response response = client.newCall(request).execute()
        def content = response.body().string()
        assert content.contains("a plane carrier order")
    }
    @Test
    void testRouteOrdersFallback(){
        String url = "http://localhost:8080/orders?_code=1"
        Request request = new Request.Builder()
                .url(url)
                .build()

        @Cleanup Response response = client.newCall(request).execute()
        def content = response.body().string()
        assert content.contains("fallback")
    }
}
