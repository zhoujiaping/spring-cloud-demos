package org.example.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * circuit:电路
 */
@Configuration
public class RouteConfig {
    @Bean
    public RouteLocator customRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p ->
                        p.path("/members")
                                .filters(f ->
                                        f.addRequestHeader("hello", "world")
                                                .circuitBreaker(config -> config.setName("mycmd").setFallbackUri("forward:/fallback")))
                                .uri("http://localhost:8081")

                )
                .route(p ->
                        p.path("/orders")
                                .filters(f ->
                                        f.addRequestHeader("hello", "world")
                                                //响应超时 断路
                                                .circuitBreaker(config -> config.setName("mycmd").setFallbackUri("forward:/fallback")))
                                .uri("http://localhost:8082")
                )
                .route(p -> p
                        .host("*.circuitbreaker.com")
                        .filters(f -> f.circuitBreaker(config ->
                                config.setName("mycmd").setFallbackUri("forward:/fallback")))
                        .uri("http://httpbin.org:80")
                )
                .build();
    }
}
