package org.example.controller;

import lombok.SneakyThrows;
import org.example.dto.Order;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {
    @GetMapping
    public Mono<List<Order>> members(Integer _code){
        if(_code!=null && _code>0){
            throw new RuntimeException("something is failure!");
        }
        return Mono.just(List.of(
                Order.builder().orderNo("O001").description("a plane carrier order").build()
        ));
    }
    @SneakyThrows
    @GetMapping("/delay/{seconds}")
    public Mono<List<Order>> membersDelay(@PathVariable("seconds") Integer seconds){
        Thread.sleep(seconds*1000);
        return Mono.just(List.of(
                Order.builder().orderNo("O001").description("a plane carrier order").build()
        ));
    }
}
