package org.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.example.dto.Member;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/members")
@Slf4j
public class MemberController {
    @GetMapping
    public Mono<List<Member>> members(@RequestHeader String hello){
        log.info("request head: hello={}",hello);
        return Mono.just(List.of(
                Member.builder().name("jack").memberNo("M001").description("captain jack sparrow").build()
        ));
    }
}
