package org.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/")
public class EchoController {
    @GetMapping("/echo")
    public Object echo(String name){
        long r = System.currentTimeMillis()%3;
        if(r==0){
            throw new RuntimeException(String.valueOf(r));
        }
        return "hello "+name;
    }
}
