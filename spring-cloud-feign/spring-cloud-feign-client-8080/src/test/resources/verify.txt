# http://localhost:8080/echo?name=jack

验证步骤
1. 启动spring-cloud-feign-client-8080、spring-cloud-feign-server-8081、spring-cloud-feign-server-8082
2. 多次请求http://localhost:8080/remote-reactor-echo?name=jack3
3. 查看页面结果，以及三个服务的日志。

预期
两台server都有日志，证明负载均衡生效了。
如果某次请求，server有报错，而页面返回from remote:hello jack3，则说明重试生效了。
如果某次请求，server打印了异常，并且页面显示from remote:fallback，说明fallback生效了。

其他暂未验证。
