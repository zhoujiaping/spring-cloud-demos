package org.exmaple;

import io.github.resilience4j.bulkhead.Bulkhead;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.reactor.bulkhead.operator.BulkheadOperator;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import io.github.resilience4j.reactor.ratelimiter.operator.RateLimiterOperator;
import io.github.resilience4j.reactor.retry.RetryOperator;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class RetryTest {
    @Test
    public void testRetry() throws Exception {
        AtomicInteger counter = new AtomicInteger(0);
        CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults("name");
        Retry retry = Retry.of("name", RetryConfig.custom().retryOnException(e -> true)
                .maxAttempts(3)
                .retryExceptions(Exception.class)
                .build());
        Retry retry2 = Retry.ofDefaults("name");
        /*int v = retry.executeCallable(() -> {
            int count = counter.getAndIncrement();
            if (count < 2) {
                throw new RuntimeException("error...");
            }
            return count;
        });
        log.info("v=>{}",v);*/
        Bulkhead bulkhead = Bulkhead.ofDefaults("name");
        RateLimiter rateLimiter = RateLimiter.ofDefaults("name");
        Mono<Integer> result = Mono.fromCallable(() -> {
                    int count = counter.getAndIncrement();
                    if (count < 2) {
                        throw new RuntimeException("error...");
                    }
                    return count;
                })
                .transformDeferred(CircuitBreakerOperator.of(circuitBreaker))
                .transformDeferred(RetryOperator.of(retry2))
                .transformDeferred(BulkheadOperator.of(bulkhead))
                .transformDeferred(RateLimiterOperator.of(rateLimiter))
                ;
        result.subscribe(it -> log.info(it.toString()));
    }

    public static void main(String[] args) throws Exception {
        new RetryTest().testRetry();
        CountDownLatch latch = new CountDownLatch(1);
        latch.await();
    }

}
