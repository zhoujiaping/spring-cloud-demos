package org.example.aop;

import io.github.resilience4j.bulkhead.Bulkhead;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.reactor.bulkhead.operator.BulkheadOperator;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import io.github.resilience4j.reactor.ratelimiter.operator.RateLimiterOperator;
import io.github.resilience4j.reactor.retry.RetryOperator;
import io.github.resilience4j.retry.Retry;
import lombok.Lombok;
import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.example.fallback.Fallback;
import org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Aspect
@Component
public class FeignAspect {
    CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults("circuitBreakerName");
    Retry retry = Retry.ofDefaults("retryName");
    Bulkhead bulkhead = Bulkhead.ofDefaults("bulkheadName");
    RateLimiter rateLimiter = RateLimiter.ofDefaults("rateLimiterName");

    @Pointcut("@within(reactivefeign.spring.config.ReactiveFeignClient)")
    public void point() {
    }

    @Around("point()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        if (joinPoint instanceof MethodInvocationProceedingJoinPoint) {
            MethodInvocationProceedingJoinPoint point = (MethodInvocationProceedingJoinPoint) joinPoint;
            Signature signature = point.getSignature();
            if (signature instanceof MethodSignature) {
                MethodSignature methodSignature = (MethodSignature) signature;
                return invoke(point, methodSignature);
            }
        }
        return joinPoint.proceed();
    }

    Map<Class,Object> fallbackCache = new ConcurrentHashMap<>();

    @SneakyThrows
    private Object invoke(MethodInvocationProceedingJoinPoint point, MethodSignature signature) {
        Method method = signature.getMethod();
        Object[] args = point.getArgs();
        Object target = point.getTarget();
        Class<?> retType = method.getReturnType();
        Mono<?> mono = null;
        if (Mono.class.isAssignableFrom(retType)) {
            mono = ((Mono) point.proceed());
        }else if(Mono.class.isAssignableFrom(retType)){
            Flux flux = ((Flux) point.proceed());
            mono = Mono.from(flux);
        }
        if(mono==null){
            return point.proceed();
        }else {
             mono = mono.transformDeferred(CircuitBreakerOperator.of(circuitBreaker))
                    .transformDeferred(RetryOperator.of(retry))
                    .transformDeferred(BulkheadOperator.of(bulkhead))
                    .transformDeferred(RateLimiterOperator.of(rateLimiter))
                    /*.onErrorResume(e -> {
                        try {
                            return method.invoke(fallback, args);
                        } catch (Exception ex) {
                            throw Lombok.sneakyThrow(ex);
                        }
                    })*/
            ;
            Optional<Fallback> optional = findAnnotation(target.getClass(),Fallback.class);
            if(optional.isPresent()){
                mono = mono.onErrorResume(e->{
                    Class<?> fallbackClass = optional.get().value();
                    Object back = getOrCreateFallback(fallbackClass);
                    try {
                        return (Mono)method.invoke(back,args);
                    } catch (Exception ex) {
                        throw Lombok.sneakyThrow(ex);
                    }
                });
            }
            return mono;
        }
    }

    private <T extends Annotation> Optional<T> findAnnotation(Class clazz, Class<T> annoClazz) {
        Class<?>[] interfaces = clazz.getInterfaces();
        T anno;
        for(Class<?> it:interfaces){
            anno = it.getAnnotation(annoClazz);
            if(anno!=null){
                return Optional.of(anno);
            }
        }
        return Optional.empty();
    }

    @SneakyThrows
    private Object getOrCreateFallback(Class<?> fallbackClass) {
        Object fallback = fallbackCache.get(fallbackClass);
        if(fallback == null){
            synchronized (fallbackClass){
                fallback = fallbackClass.getConstructor().newInstance();
                fallbackCache.put(fallbackClass,fallback);
            }
        }
        return fallback;
    }
}
