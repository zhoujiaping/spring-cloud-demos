package org.example;

//import org.example.config.ReactiveFeignConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.reactive.config.EnableWebFlux;
import reactivefeign.spring.config.EnableReactiveFeignClients;
//import reactivefeign.spring.config.EnableReactiveFeignClients;

@SpringBootApplication
//@EnableFeignClients
@EnableWebFlux
@EnableReactiveFeignClients(/*defaultConfiguration = ReactiveFeignConfig.class*/)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}