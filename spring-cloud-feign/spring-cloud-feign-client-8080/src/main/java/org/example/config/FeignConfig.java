package org.example.config;

import feign.Feign;
import feign.Logger;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class FeignConfig {
    @Bean
    @Scope("prototype")
    @ConditionalOnMissingBean
    public Feign.Builder feignResilience4jBuilder() {
        Feign.Builder builder = new Resilience4jFeign.Builder(new FeignDecorators.Builder()
                //重试
                .withRetry(Retry.of("default", RetryConfig.ofDefaults()))
                //舱壁/隔板(bulkhead)模式  实际上就是线程资源隔离的一种
                //.withBulkhead()
                //断路器(circuit breaker)模式
                //.withCircuitBreaker()
                //后备(fallback)模式
                //.withFallback()
                //限流
                //.withRateLimiter()
                .build()
        );
        //配置client，可以使用连接池、负载均衡(可以使用spring-cloud-starter-loadbalancer)
        //配置requestInterceptor，可以使用请求拦截
        //配置invocationHandlerFactory，可以使用服务发现

        //builder.client().decoder().encoder().invocationHandlerFactory().requestInterceptor().retryer();
        return builder.logLevel(Logger.Level.FULL);
    }
}
