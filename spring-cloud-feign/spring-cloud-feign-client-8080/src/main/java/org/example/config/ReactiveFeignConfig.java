//package org.example.config;
//
//import feign.Contract;
//import io.github.resilience4j.bulkhead.Bulkhead;
//import io.github.resilience4j.circuitbreaker.CircuitBreaker;
//import io.github.resilience4j.ratelimiter.RateLimiter;
//import io.github.resilience4j.reactor.bulkhead.operator.BulkheadOperator;
//import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
//import io.github.resilience4j.reactor.ratelimiter.operator.RateLimiterOperator;
//import io.github.resilience4j.reactor.retry.RetryOperator;
//import io.github.resilience4j.retry.Retry;
//import lombok.Lombok;
//import lombok.extern.slf4j.Slf4j;
//import org.example.feign.ReactorEchoFeign;
//import org.example.feign.ReactorEchoFeignFallback;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cglib.proxy.Enhancer;
//import org.springframework.cglib.proxy.InvocationHandler;
//import org.springframework.cloud.openfeign.support.SpringMvcContract;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//import org.springframework.web.reactive.function.client.WebClient;
//import reactivefeign.ReactiveOptions;
//import reactivefeign.webclient.WebClientFeignCustomizer;
//import reactivefeign.webclient.WebReactiveFeign;
//import reactivefeign.webclient.WebReactiveOptions;
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;
//
//
//这个配置也可以，不过没有PlaytikaReactiveFeignConfig那么优雅
//@Configuration
//@Slf4j
//public class ReactiveFeignConfig {
//
//    @Bean
//    @Scope("prototype")
//    public WebReactiveFeign.Builder reactiveFeignBuilder(
//            WebClient.Builder builder,
//            @Autowired(required = false) WebClientFeignCustomizer webClientCustomizer) {
//        return webClientCustomizer != null
//                ? WebReactiveFeign.builder(builder, webClientCustomizer)
//                : WebReactiveFeign.builder(builder);
//    }
//
//    @Bean
//    public Contract feignContract() {
//        return new SpringMvcContract();
//    }
//
//    @Bean
//    public ReactorEchoFeign reactorEchoFeign(WebReactiveFeign.Builder<ReactorEchoFeign> reactiveFeignBuilder) {
//        CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults("circuitBreakerName");
//        Retry retry = Retry.ofDefaults("retryName");
//        Bulkhead bulkhead = Bulkhead.ofDefaults("bulkheadName");
//        RateLimiter rateLimiter = RateLimiter.ofDefaults("rateLimiterName");
//        String service = "http://spring-cloud-feign-server/";
//        //String service = "http://localhost:8081/";
//        ReactiveOptions reactiveOptions = new WebReactiveOptions.Builder()
//                .setReadTimeoutMillis(2200L)
//                .setWriteTimeoutMillis(2200L)
//                .setConnectTimeoutMillis(5000L)
//                .build();
//        ReactorEchoFeign feign = //WebReactiveFeign.<ReactorEchoFeign>builder()
//                reactiveFeignBuilder
//                .options(reactiveOptions)
//                .contract(feignContract())
//                //不能直接用这个fallback，这个先于retry
//                //.fallbackFactory(e->new ReactorEchoFeignFallback())
//                .target(ReactorEchoFeign.class, service)
//                ;
//
//        ReactorEchoFeignFallback fallback = new ReactorEchoFeignFallback();
//        ReactorEchoFeign enhanced = (ReactorEchoFeign) Enhancer.create(ReactorEchoFeign.class, (InvocationHandler) (proxy, method, args) -> {
//            Class<?> retType = method.getReturnType();
//            if (Mono.class.isAssignableFrom(retType)) {
//                Mono<?> mono = ((Mono) method.invoke(feign, args))
//                        .transformDeferred(CircuitBreakerOperator.of(circuitBreaker))
//                        .transformDeferred(RetryOperator.of(retry))
//                        .transformDeferred(BulkheadOperator.of(bulkhead))
//                        .transformDeferred(RateLimiterOperator.of(rateLimiter)
//                        ).onErrorResume(e -> {
//                            try {
//                                return method.invoke(fallback, args);
//                            } catch (Exception ex) {
//                               throw Lombok.sneakyThrow(ex);
//                            }
//                        });
//                return mono;
//            }else if(Flux.class.isAssignableFrom(retType)){
//                Flux<?> flux = ((Flux) method.invoke(feign, args))
//                        .transformDeferred(CircuitBreakerOperator.of(circuitBreaker))
//                        .transformDeferred(RetryOperator.of(retry))
//                        .transformDeferred(BulkheadOperator.of(bulkhead))
//                        .transformDeferred(RateLimiterOperator.of(rateLimiter)
//                        ).onErrorResume(e -> {
//                            try {
//                                return method.invoke(fallback, args);
//                            } catch (Exception ex) {
//                                throw Lombok.sneakyThrow(ex);
//                            }
//                        });
//                return flux;
//            } else {
//                return method.invoke(feign, args);
//            }
//        });
//        return enhanced;
//    }
//}
