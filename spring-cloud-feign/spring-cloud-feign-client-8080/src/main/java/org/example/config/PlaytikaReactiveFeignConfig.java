//package org.example.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.reactive.function.client.WebClient;
//import reactivefeign.ReactiveFeignBuilder;
//import reactivefeign.ReactiveOptions;
//import reactivefeign.retry.BasicReactiveRetryPolicy;
//import reactivefeign.retry.ReactiveRetryPolicy;
//import reactivefeign.spring.config.ReactiveRetryPolicies;
//import reactivefeign.webclient.WebReactiveFeign;
//import reactivefeign.webclient.WebReactiveOptions;
//
//@Configuration
//@Slf4j
//public class PlaytikaReactiveFeignConfig {
//    @Bean
//    public ReactiveOptions.Builder reactiveOptionsBuilder(){
//        return new WebReactiveOptions.Builder().setUseHttp2(true)
//                .setAcceptCompressed(true)
//                .setConnectTimeoutMillis(1000)
//                .setFollowRedirects(true)
//                //.setProxySettings()
//                ;
//    }
//    @Bean
//    public ReactiveRetryPolicies reactiveRetryPolicies(){
//        ReactiveRetryPolicy.Builder policy = new BasicReactiveRetryPolicy.Builder()
//                .setMaxRetries(2)
//                //重试间隔
//                .setBackoffInMs(10);
//        return new ReactiveRetryPolicies.Builder().retryOnNext(policy.build()).build();
//    }
//    //List<Class<ReactiveHttpRequestInterceptor>>,
//    //ReactiveStatusHandler,
//    //feign.codec.ErrorDecoder,
//    //ReactiveLoggerListener,
//    //MicrometerReactiveLogger
//}
