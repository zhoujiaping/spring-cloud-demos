package org.example.feign;

public class EchoFeignFallback implements EchoFeign {
    @Override
    public String echo(String name) {
        return "fallback";
    }
}
