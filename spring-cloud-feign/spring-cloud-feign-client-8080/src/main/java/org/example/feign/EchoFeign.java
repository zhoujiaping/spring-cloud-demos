package org.example.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="echoFeign",url = "http://localhost:8081",fallback = EchoFeignFallback.class)
public interface EchoFeign {
    @GetMapping("/echo")
    String echo(@RequestParam("name") String name);
}
