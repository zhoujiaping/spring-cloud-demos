package org.example.feign;

import reactor.core.publisher.Mono;

public class ReactorEchoFeignFallback implements ReactorEchoFeign {
    @Override
    public Mono<String> echo(String name) {
        return Mono.just("fallback");
    }
}
