package org.example.feign;

import feign.Headers;
import feign.RequestLine;
import org.example.fallback.Fallback;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
//import reactivefeign.spring.config.ReactiveFeignClient;
import reactivefeign.spring.config.ReactiveFeignClient;
import reactor.core.publisher.Mono;

/**
 * 这个fallback优先级比调用服务方法时指定的retry（以及限流等）高，
 * 所以需要自己通过aop或者其他方式实现。
 * */
@ReactiveFeignClient(name="reactorEchoFeign"/*,fallback = ReactorEchoFeignFallback.class*/)
@Fallback(ReactorEchoFeignFallback.class)
//@Headers({"Content-Type:application/json"})
public interface ReactorEchoFeign {
    @GetMapping("/echo")
    Mono<String> echo(@RequestParam("name") String name);

}

