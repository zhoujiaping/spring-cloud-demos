package org.example.controller;

import io.github.resilience4j.bulkhead.Bulkhead;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.retry.Retry;
import org.example.feign.EchoFeign;
import org.example.feign.ReactorEchoFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/")
public class EchoController {
    //@Autowired
    EchoFeign echoFeign;
    @Autowired
    ReactorEchoFeign reactorEchoFeign;

    CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults("name");
    Retry retry = Retry.ofDefaults("backendName");
    Bulkhead bulkhead = Bulkhead.ofDefaults("name");
    RateLimiter rateLimiter = RateLimiter.ofDefaults("name");

    @GetMapping("/echo")
    public Object echo(String name) {
        return "hello " + name;
    }

    @GetMapping("/remote-echo")
    public Object remoteEcho(String name) {
        return "from remote: " + echoFeign.echo(name);
    }

    @GetMapping("/remote-reactor-echo")
    public Mono<String> remoteReactorEcho(String name) {
        return reactorEchoFeign.echo(name).map(msg ->
                        "from remote: " + msg
                )
                //后面的包裹在外面
//                .transformDeferred(CircuitBreakerOperator.of(circuitBreaker))
//                .transformDeferred(RetryOperator.of(retry))
//                .transformDeferred(BulkheadOperator.of(bulkhead))
//                .transformDeferred(RateLimiterOperator.of(rateLimiter))
                //.onErrorResume(e -> ((ReactorEchoFeign) Fallbacks.of(reactorEchoFeign.getClass())).echo(name))
                ;
    }
}
