nacos官网
https://nacos.io/zh-cn/docs/quick-start.html

windows下单节点启动
startup.cmd -m standalone

nacos控制台
http://192.168.0.108:8848/nacos/index.html
可以管理配置、服务、集群、权限、命名空间

启动spring-cloud-discovery-nacos-server-8082
在nacos控制台可以看到新注册了一个example-server的服务

启动spring-cloud-discovery-nacos-client-8080
在nacos控制台可以看到新注册了一个spring-cloud-discovery-nacos-client的服务

访问http://localhost:8080/echo?str=abc
响应local:abc

访问http://localhost:8080/echo/abc
响应hello abc
可能响应Connection refused，那是因为负载均衡，我们还没有启动另一个服务节点。

spring-cloud-discovery-nacos-client-8090整合了
