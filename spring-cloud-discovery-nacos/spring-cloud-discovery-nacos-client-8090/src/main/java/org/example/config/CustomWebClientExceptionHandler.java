package org.example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

//import javax.validation.ConstraintViolationException;
import java.util.Map;

/**
 * 全局异常处理
 * */
@RestControllerAdvice
public class CustomWebClientExceptionHandler {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(Exception.class)
    @ResponseStatus(code = HttpStatus.OK)
    public Mono<Map> handleCustomException(Exception e) {
        logger.error("",e);
        return Mono.just(Map.of("code","500","msg",e.getMessage()));
    }

    /**处理验证框架抛出的异常*/
    /*
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(code = HttpStatus.OK)
    public Mono<Map> handleClientException(Exception e) {
        logger.error("",e);
        return Mono.just(Map.of("code","400"));
    }*/

}