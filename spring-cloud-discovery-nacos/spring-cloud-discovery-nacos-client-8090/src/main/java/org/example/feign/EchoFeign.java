package org.example.feign;

import org.example.fallback.Fallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactivefeign.spring.config.ReactiveFeignClient;
import reactor.core.publisher.Mono;

@ReactiveFeignClient("echo-feign-client")
@Fallback(EchoFeignFallback.class)
public interface EchoFeign {
    @GetMapping("/echo/{str}")
    Mono<String> echo(@PathVariable("str") String str);
}
