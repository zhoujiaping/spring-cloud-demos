package org.example.feign;

import reactor.core.publisher.Mono;

public class EchoFeignFallback implements EchoFeign{
    @Override
    public Mono<String> echo(String str) {
        return Mono.just("fallback");
    }
}
