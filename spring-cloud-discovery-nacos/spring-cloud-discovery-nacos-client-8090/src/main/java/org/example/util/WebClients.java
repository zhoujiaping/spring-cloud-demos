package org.example.util;

import io.github.resilience4j.bulkhead.Bulkhead;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.reactor.bulkhead.operator.BulkheadOperator;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import io.github.resilience4j.reactor.ratelimiter.operator.RateLimiterOperator;
import io.github.resilience4j.reactor.retry.RetryOperator;
import io.github.resilience4j.retry.Retry;
import reactor.core.publisher.Mono;
import java.util.function.Supplier;

public abstract class WebClients {
    static CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults("circuitBreakerName");
    static Retry retry = Retry.ofDefaults("retryName");
    static Bulkhead bulkhead = Bulkhead.ofDefaults("bulkheadName");
    static RateLimiter rateLimiter = RateLimiter.ofDefaults("rateLimiterName");

    /**
     * 提供一个快捷方式，应用重试、限流等
     * @param fn
     * @param <T>
     * @return
     */
    public static <T> Mono<T> apply(Supplier<Mono<T>> fn){
        return fn.get().transformDeferred(CircuitBreakerOperator.of(circuitBreaker))
                .transformDeferred(RetryOperator.of(retry))
                .transformDeferred(BulkheadOperator.of(bulkhead))
                .transformDeferred(RateLimiterOperator.of(rateLimiter));
    }
}
