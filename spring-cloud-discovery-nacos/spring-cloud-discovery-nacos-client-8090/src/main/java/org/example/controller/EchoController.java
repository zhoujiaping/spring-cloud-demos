package org.example.controller;

import org.example.feign.EchoFeign;
import org.example.util.WebClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.reactive.ReactorLoadBalancerExchangeFilterFunction;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
public class EchoController {
    @Autowired
    WebClient webClient;
    @Autowired
    EchoFeign echoFeign;

    @GetMapping("/echo")
    public Mono<String> localEcho(String str) {
        return Mono.just("local:"+str);
    }

    @GetMapping("/echo/{str}")
    public Mono<String> echo(@PathVariable String str) {
        return WebClients.apply(()->webClient.get().uri("http://example-server/echo/" + str)
                .retrieve().bodyToMono(String.class))
                //fallback
                .onErrorResume(e->Mono.just(e.getMessage()));
    }
    /**
     * 整合openfeign
     * */
    @GetMapping("/feign-echo/{str}")
    public Mono<String> feignEcho(@PathVariable String str){
        return echoFeign.echo(str);
    }
}