package org.example.controller;

import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
public class EchoController {

    @GetMapping("/echo/{str}")
    public Mono<String> echo(@PathVariable String str) {
        long millis = System.currentTimeMillis();
        if (millis % 3 == 0) {
            return Mono.just("hello(8081) " + str);
        }
        throw new RuntimeException("exception(8081)...");
    }

}