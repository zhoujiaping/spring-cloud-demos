package org.example.config;

import io.netty.channel.ChannelOption;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.loadbalancer.core.RandomLoadBalancer;
import org.springframework.cloud.loadbalancer.core.ReactorLoadBalancer;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;
import reactor.netty.resources.LoopResources;

import java.time.Duration;

@Slf4j
@Configuration
@LoadBalancerClient(name = "load-balancer-client"/*, configuration = ExampleServiceConfig.class*/)
public class WebClientConfig {
    /**
     * https://spring.io/guides/gs/spring-cloud-loadbalancer/
     * loadbalancer支持resttemplate、webclient
     */
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @LoadBalanced
    @Bean
    WebClient.Builder webClientBuilder(WebClientCustomizer webClientCustomizer) {
        WebClient.Builder builder = WebClient.builder();
        webClientCustomizer.customize(builder);
        return builder;
    }

    @Bean
    public WebClient webClient(WebClient.Builder builder){
        return builder.build();
    }

    /**
     * 内置轮询、随机的负载均衡策略，默认轮询策略。
     * 可以通过 LoadBalancerClient 注解，指定服务级别的负载均衡策略
     * @param loadBalancerClientFactory
     * @return
     */
    @Bean
    public ReactorLoadBalancer<ServiceInstance> reactorServiceInstanceLoadBalancer(
                                                                                   LoadBalancerClientFactory loadBalancerClientFactory) {
        String name = "example-server";
        //对example-server这个微服务使用随机策略
        return new RandomLoadBalancer(
                loadBalancerClientFactory.getLazyProvider(name, ServiceInstanceListSupplier.class), name);
    }
    /**webclient配置*/
    @Bean
    public WebClientCustomizer webClientCustomizer(){
        //配置动态连接池
        //ConnectionProvider provider = ConnectionProvider.elastic("elastic pool");
        //配置固定大小连接池，如最大连接数、连接获取超时、空闲连接死亡时间等
        var provider = ConnectionProvider.builder("fixed").maxConnections(45)
                .evictInBackground(Duration.ofSeconds(20))
                .maxIdleTime(Duration.ofSeconds(6))
                .pendingAcquireTimeout(Duration.ofSeconds(4))
                .pendingAcquireMaxCount(100)
                .maxLifeTime(Duration.ofSeconds(60))
                .build();
        //ConnectionProvider provider = ConnectionProvider.fixed("fixed", 45, 4000, Duration.ofSeconds(6));
        LoopResources loop = LoopResources.create("kl-event-loop", 1, 4, true);
        HttpClient httpClient = HttpClient.create(provider)
                /* .secure(sslContextSpec -> {
                     SslContextBuilder sslContextBuilder = SslContextBuilder.forClient()
                             .trustManager(new File("E://server.truststore"));
                     sslContextSpec.sslContext(sslContextBuilder);
                 })*/
                .keepAlive(true)
                .responseTimeout(Duration.ofSeconds(10))
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000)
                .option(ChannelOption.TCP_NODELAY, true)
                .runOn(loop);
        return webClientBuilder-> {
            log.info("webClientBuilder");
            webClientBuilder.clientConnector(new ReactorClientHttpConnector(httpClient));
        };
    }
}
