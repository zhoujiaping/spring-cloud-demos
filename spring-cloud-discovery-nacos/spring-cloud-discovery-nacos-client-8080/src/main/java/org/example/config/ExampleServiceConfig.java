//package org.example.config;
//
//import org.springframework.cloud.client.DefaultServiceInstance;
//import org.springframework.cloud.client.ServiceInstance;
//import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import reactor.core.publisher.Flux;
//
//import java.util.Arrays;
//import java.util.List;
//
//@Configuration
//public class ExampleServiceConfig {
//    /**服务实例提供者
//    我们使用了nacos服务发现，所以不需要hardcode服务提供者了。
//    */
//    @Bean
//    @Primary
//    ServiceInstanceListSupplier serviceInstanceListSupplier() {
//        return new ExampleServiceInstanceListSuppler("example-server");
//    }
//
//    class ExampleServiceInstanceListSuppler implements ServiceInstanceListSupplier {
//
//        private final String serviceId;
//
//        ExampleServiceInstanceListSuppler(String serviceId) {
//            this.serviceId = serviceId;
//        }
//
//        @Override
//        public String getServiceId() {
//            return serviceId;
//        }
//
//        @Override
//        public Flux<List<ServiceInstance>> get() {
//            return Flux.just(Arrays
//                    .asList(new DefaultServiceInstance(serviceId + "1", serviceId, "localhost", 8081, false),
//                            new DefaultServiceInstance(serviceId + "3", serviceId, "localhost", 8082, false)
//                    )
//            );
//        }
//    }
//}