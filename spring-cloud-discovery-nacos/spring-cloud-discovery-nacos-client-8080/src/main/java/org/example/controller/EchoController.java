package org.example.controller;

import org.example.feign.EchoFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.reactive.ReactorLoadBalancerExchangeFilterFunction;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
public class EchoController {
    @Autowired
    WebClient webClient;
    @Autowired
    EchoFeign echoFeign;
    //@Autowired
    //WebClient.Builder loadBalancedWebClientBuilder;
    //@Autowired
    //ReactorLoadBalancerExchangeFilterFunction lbFunction;
    @GetMapping("/echo")
    public Mono<String> localEcho(String str) {
        return Mono.just("local:"+str);
    }
   /* @GetMapping("/echo/{str}")
    public String echo(@PathVariable String str) {
        //异常：Spring WebFlux(Reactor3) block()/blockFirst()/blockLast()
        //在并行或单个的Scheduler中调用阻塞的方法就会抛出异常了。原因在更新日志中也写明了：阻塞会影响线程池中有限的资源，而且还有很大概率冻结程序的风险。
        return restTemplate.getForObject("http://example-server/echo/" + str, String.class);
    }*/
    @GetMapping("/echo/{str}")
    public Mono<String> echo(@PathVariable String str) {
        /**restTemplate是上面加了LoadBalance注解，它会解析http://example/echo/{str}得到服务名example，\
         * 然后去nacos查询该服务的提供者，然后调用服务提供者对应的rest api。
         * */
        //可以debug查看webClient的builder，是否有配置clientConnector，验证是否应用了连接池配置
        return webClient.get().uri("http://example-server/echo/" + str)
                .retrieve().bodyToMono(String.class);
    }
    /**
     * 整合openfeign
     * */
    @GetMapping("/feign-echo/{str}")
    public Mono<String> feignEcho(@PathVariable String str){
        return echoFeign.echo(str);
    }

    /**
     * LoadBalancerClient 配置了name=example
     * @param name
     * @return
     */
/*    @RequestMapping("/hello")
    public Mono<String> hello(@RequestParam(value = "name", defaultValue = "John") String name) {
        return loadBalancedWebClientBuilder
                .filter(lbFunction)
                .build().get().uri("http://example/echo/"+name)
                .retrieve().bodyToMono(String.class)
                .map(greeting -> String.format("%s, %s!", greeting, name));
    }*/
}