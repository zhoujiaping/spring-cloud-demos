package org.example.pojo;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.example.common.jdbc.HisTable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table("t_order")
public class Order {
    @Id
    Long id;
    Long userId;
    String orderNo;
    Long goodsId;
    Long deliveryAddrId;
    BigDecimal price;
    String status;
    Long createdAt;
    Long lastUpdatedAt;
    @Version
    Integer version;
}
