package org.example.pojo;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table("t_local_msg")
public class LocalMsg {
    @Id
    Long id;
    String bizType;
    String content;
    String status;
    Long createdAt;
    Long lastUpdatedAt;
    @Version
    Integer version;
}
