package org.example.pojo;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table("t_coupon")
public class Coupon {
    @Id
    Long id;
    Long userId;
    Integer amount;
    Long createdAt;
    Long lastUpdatedAt;
    @Version
    Integer version;
}
