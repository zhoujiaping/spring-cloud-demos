package org.example.repository;

import org.example.common.jdbc.JdbcRepo;
import org.example.pojo.LocalMsg;
import org.example.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class LocalMsgRepository extends JdbcRepo<LocalMsg> {
    @Autowired
    public LocalMsgRepository(DataSource dataSource){
        setDataSource(dataSource);
    }
}
