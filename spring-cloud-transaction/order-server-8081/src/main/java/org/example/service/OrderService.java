package org.example.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.example.pojo.Coupon;
import org.example.pojo.LocalMsg;
import org.example.pojo.Order;
import org.example.repository.LocalMsgRepository;
import org.example.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.IdGenerator;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

@Service
public class OrderService {
    @Autowired
    TxService txService;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    LocalMsgRepository localMsgRepository;
    @Autowired
    WebClient webClient;
    ObjectMapper objectMapper = new ObjectMapper();

    public Order create(Order order) {
        Long msgId = txService.withTx(() -> {
            //订单入库
            order.setUserId(-1L);
            order.setGoodsId(-1L);
            order.setPrice(new BigDecimal("1000"));
            order.setDeliveryAddrId(-1L);
            orderRepository.insertSelective(order);
            //新增本地消息
            LocalMsg localMsg = LocalMsg.builder().bizType("create-order")
                    .content(toJson(order)).build();
            return localMsgRepository.insertSelective(localMsg);
        });
        //rpc调用，更新积分
        Mono<Coupon> couponMono = webClient.post().uri("http://localhost:8080/coupons")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(toJson(order)).retrieve().bodyToMono(Coupon.class);
        couponMono.subscribe(coupon -> {
            txService.withTx(() -> {
                //更新本地消息为COMMITTED
                localMsgRepository.updateByIdSelective(LocalMsg.builder()
                        .id(msgId).status("COMMITTED")
                        .build());
                return null;
            });
        });
        //然后有个调度轮询本地消息表，重发长时间处于PREPARED状态的消息。
        return order;
    }

    @SneakyThrows
    private String toJson(Object obj) {
        return objectMapper.writeValueAsString(obj);
    }
}
