package org.example.service;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Supplier;

@Component
public class TxService {
    @Transactional(rollbackFor = Throwable.class)
    public <T> T withTx(Supplier<T> fn){
        return fn.get();
    }
}
