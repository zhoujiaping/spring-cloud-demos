package org.example.util;

import java.util.Map;

public abstract class Strs {
    public static String format(String tmpl, Map<String,Object> kvs){
        return new GenericTokenParser("{","}",key-> String.valueOf(kvs.get(key))).parse(tmpl);
    }
}
