package org.example.common.jdbc;

import org.springframework.jdbc.support.JdbcUtils;

import java.util.List;
import java.util.function.Function;

public abstract class Sqls {
    public static  <P> int doInBatch(List<P> list, int batchSize, Function<List<P>, Integer> fn) {
        int from = 0, to = Math.min(list.size(), batchSize);
        int count = 0;
        while (from < to) {
            List<P> sublist = list.subList(from, to);
            count += fn.apply(sublist);
            from = to;
            to = Math.min(list.size(), to + batchSize);
        }
        return count;
    }

    public static String toColumnName(String uncapitalizedName) {
        var sb = new StringBuilder();
        var chs = uncapitalizedName.toCharArray();
        for(char ch: chs){
            if(Character.isUpperCase(ch)){
                sb.append('_').append(Character.toLowerCase(ch));
            }else{
                sb.append(ch);
            }
        }
        return sb.toString();
        //return uncapitalizedName.replaceAll("(?<Uper>[A-Z])", "_${Uper}").toLowerCase();
    }
    public static String toPropName(String underScopeName){
        return JdbcUtils.convertUnderscoreNameToPropertyName(underScopeName);
    }
}
