package org.example.common.jdbc;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.List;
import java.util.Set;

interface BaseRepo<ENTITY> {
    /**查询所有*/
    List<ENTITY> query();
    /**根据主键查询*/
    ENTITY query(Long id);
    /**根据id集合查询*/
    List<ENTITY> query(List<Long> id);
    /**新增*/
    Long insertSelective(ENTITY t);
    /**更新*/
    int updateByIdSelective(ENTITY t);
    /**乐观锁更新*/
    int updateByIdSelectiveWithOptiLock(ENTITY t);
    /**批量新增*/
    int batchInsert(List<String> includePropNames, List<ENTITY> list, int batchSize);
    default int batchInsert(List<String> includePropNames, List<ENTITY> list){
        return batchInsert(includePropNames,list,1000);
    }
    /**批量更新*/
    int batchUpdateById(List<String> includePropNames, List<ENTITY> list, int batchSize);
    default int batchUpdateById(List<String> includePropNames, List<ENTITY> list){
        return batchUpdateById(includePropNames,list,1000);
    }
    int batchUpdateByIdForExclude(Set<String> excludePropNames, List<ENTITY> list, int batchSize);
    default int batchUpdateByIdForExclude(Set<String> excludePropNames, List<ENTITY> list){
        return batchUpdateByIdForExclude(excludePropNames,list,1000);
    }
    /**批量更新*/
    int batchUpdateByIdWithOptiVersion(List<String> includePropNames, List<ENTITY> list, int batchSize);
    default int batchUpdateByIdWithOptiVersion(List<String> includePropNames, List<ENTITY> list){
        return batchUpdateByIdWithOptiVersion(includePropNames,list,1000);
    }
    /**新增或者更新*/
    <R> PageRes<R> queryByPage(String sql, SqlParameterSource paramSource, Class<R> clazz, PageParam pageParam);
    /**根据id删除*/
    int deleteById(Long id);
    /**批量删除*/
    int batchDeleteById(List<Long> ids, int batchSize);
    default int batchDeleteById(List<Long> ids){
        return batchDeleteById(ids,1000);
    }
}