package org.example.common.jdbc;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageParam {
    Integer page;
    Integer size;
    public int offset(){
        return (page-1)*size;
    }
}
