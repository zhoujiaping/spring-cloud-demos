package org.example.common.jdbc;

import org.example.util.Strs;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.util.StringUtils;

import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

public class JdbcRepo< ENTITY> extends NamedParameterJdbcDaoSupport
        implements BaseRepo<ENTITY> {
    protected Class<ENTITY> entityClazz;
    protected Class<Long> idClass;
    protected RowMapper<ENTITY> rowMapper;
    protected String tableName;
    protected String hisTableName;
    protected Mappings mappings;
    protected IdMapping idMapping;
    protected VersionMapping versionMapping;

    public JdbcRepo(Class<ENTITY> entityClazz){
        init(entityClazz);
    }
    public JdbcRepo(){
        init(setupEntityClass());
    }
    public void init(Class<ENTITY> entityClazz){
        this.entityClazz = entityClazz;
        tableName = entityClazz.getAnnotation(Table.class).value();
        HisTable hisTable = entityClazz.getAnnotation(HisTable.class);
        if(hisTable!=null){
            hisTableName = hisTable.value();
        }
        rowMapper = new BeanPropertyRowMapper(entityClazz);
        mappings = Mappings.fromEntityClass(entityClazz);
        mappings.getAllMapping().forEach(mapping -> {
            if(mapping instanceof IdMapping){
                idMapping = (IdMapping) mapping;
                idClass = (Class<Long>) idMapping.getField().getType();
            }else if(mapping instanceof VersionMapping){
                versionMapping = (VersionMapping) mapping;
            }
        });
    }

    protected Class<ENTITY> setupEntityClass() {
        ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        return (Class<ENTITY>) type.getActualTypeArguments()[0];
    }

    @Override
    public List<ENTITY> query() {
        var sql = Strs.format("select * from {table}", Map.of("table", tableName));
        return getNamedParameterJdbcTemplate().query(sql, Map.of(), rowMapper);
    }

    @Override
    public ENTITY query(Long id) {
        var sql = Strs.format("select * from {table} where {idColumn}=:id",
                Map.of("table", tableName,
                        "idColumn", idMapping.getColumn()));
        return getNamedParameterJdbcTemplate().queryForObject(sql, Map.of("id", id), rowMapper);
    }

    @Override
    public List<ENTITY> query(List<Long> ids) {
        var sql = Strs.format("select * from {table} where {idColumn}in(:ids)",
                Map.of("table", tableName,
                        "idColumn", idMapping.getColumn()));
        return getNamedParameterJdbcTemplate().query(sql, Map.of("ids", ids), rowMapper);
    }

    @Override
    public Long insertSelective(ENTITY entity) {
        var acc = new DirectFieldAccessor(entity);
        Long id = (Long) acc.getPropertyValue(idMapping.getProp());
        String columns = mappings.getAllMapping().stream()
                .filter(mapping -> acc.getPropertyValue(mapping.getProp()) != null)
                .map(it -> it.getColumn())
                .collect(Collectors.joining(","));
        String values = mappings.getAllMapping().stream()
                .filter(mapping -> acc.getPropertyValue(mapping.getProp()) != null)
                .map(it -> ":" + it.getProp())
                .collect(Collectors.joining(","));
        var sql = Strs.format("insert into {table}({columns})values({values})",
                Map.of("table", tableName,
                        "columns", columns,
                        "values", values));
        if (id != null) {
            getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(entity));
            return id;
        } else {
            var kh = new GeneratedKeyHolder();
            getNamedParameterJdbcTemplate().update(sql,
                    new BeanPropertySqlParameterSource(entity), kh, new String[]{idMapping.getColumn()});
            return kh.getKey().longValue();
        }
    }

    @Override
    public int updateByIdSelective(ENTITY entity) {
        var acc = new DirectFieldAccessor(entity);
        String setters = mappings.getAllMapping().stream()
                .filter(mapping -> !mapping.getColumn().equals(idMapping.getColumn()))
                .filter(mapping -> !mapping.getColumn().equals(versionMapping.getColumn()))
                .filter(mapping -> acc.getPropertyValue(mapping.getProp()) != null)
                .map(it -> it.getColumn() + "=:" + it.getProp())
                .collect(Collectors.joining(","));
        var sql = Strs.format("update {table} set {setters},{versionColumn}={versionColumn}+1 where {idColumn}=:{idProp}",
                Map.of("table", tableName,
                        "setters", setters,
                        "versionColumn", versionMapping.getColumn(),
                        "idColumn", idMapping.getColumn(),
                        "idProp", idMapping.getProp()));
        return getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(entity));
    }

    @Override
    public int updateByIdSelectiveWithOptiLock(ENTITY entity) {
        var acc = new DirectFieldAccessor(entity);
        String setters = mappings.getAllMapping().stream()
                .filter(mapping -> !mapping.getColumn().equals(idMapping.getColumn()))
                .filter(mapping -> !mapping.getColumn().equals(versionMapping.getColumn()))
                .filter(mapping -> acc.getPropertyValue(mapping.getProp()) != null)
                .map(it -> it.getColumn() + "=:" + it.getProp())
                .collect(Collectors.joining(","));
        var sql = Strs.format("update {table} set {setters},{versionColumn}={versionColumn}+1 where {idColumn}=:{idProp} and {versionColumn}=:{versionProp}",
                Map.of("table", tableName,
                        "setters", setters,
                        "versionColumn", versionMapping.getColumn(),
                        "versionProp", versionMapping.getProp(),
                        "idColumn", idMapping.getColumn(),
                        "idProp", idMapping.getProp()));
        return getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(entity));
    }

    @Override
    public int batchInsert(List<String> includePropNames, List<ENTITY> list, int batchSize) {
        String placeHolders = includePropNames.stream()
                .map(it -> ":" + it)
                .collect(Collectors.joining(","));
        String columns = includePropNames.stream()
                .map(it -> mappings.getPropName2Mapping().get(it).getColumn())
                .collect(Collectors.joining(","));
        return Sqls.doInBatch(list, batchSize, chunk -> {
            var sources = chunk.stream().map(BeanPropertySqlParameterSource::new).toArray(size -> new BeanPropertySqlParameterSource[size]);
            var sql = Strs.format("insert into {table}({columns})values({values})",
                    Map.of("table", tableName,
                            "columns", columns,
                            "values", placeHolders));
            getNamedParameterJdbcTemplate().batchUpdate(sql, sources);
            return chunk.size();
        });
    }

    @Override
    public int batchUpdateById(List<String> includePropNames, List<ENTITY> list, int batchSize) {
        String setters = includePropNames.stream()
                .map(it->mappings.getPropName2Mapping().get(it))
                .filter(mapping -> !mapping.getColumn().equals(idMapping.getColumn()))
                .filter(mapping -> !mapping.getColumn().equals(versionMapping.getColumn()))
                .map(it -> it.getColumn() + "=:" + it.getProp())
                .collect(Collectors.joining(","));
        return Sqls.doInBatch(list, batchSize, chunk -> {
            var sources = chunk.stream().map(BeanPropertySqlParameterSource::new)
                    .toArray(size -> new BeanPropertySqlParameterSource[size]);
            var sql = Strs.format("update {table} set {setters},{versionColumn}={versionColumn}+1 where {idColumn}=:{idProp}",
                    Map.of("table", tableName,
                            "setters", setters,
                            "versionColumn", versionMapping.getColumn(),
                            "idColumn", idMapping.getColumn(),
                            "idProp", idMapping.getProp()));
            getNamedParameterJdbcTemplate().batchUpdate(sql, sources);
            return chunk.size();
        });
    }
    @Override
    public int batchUpdateByIdForExclude(Set<String> excludePropNames, List<ENTITY> list, int batchSize) {
        String setters = mappings.getPropName2Mapping().values().stream()
                .filter(mapping -> !mapping.getColumn().equals(idMapping.getColumn()))
                .filter(mapping -> !mapping.getColumn().equals(versionMapping.getColumn()))
                .filter(mapping -> !excludePropNames.contains(mapping.getProp()))
                .map(it -> it.getColumn() + "=:" + it.getProp())
                .collect(Collectors.joining(","));
        return Sqls.doInBatch(list, batchSize, chunk -> {
            var sources = chunk.stream().map(BeanPropertySqlParameterSource::new)
                    .toArray(size -> new BeanPropertySqlParameterSource[size]);
            var sql = Strs.format("update {table} set {setters},{versionColumn}={versionColumn}+1 where {idColumn}=:{idProp}",
                    Map.of("table", tableName,
                            "setters", setters,
                            "versionColumn", versionMapping.getColumn(),
                            "idColumn", idMapping.getColumn(),
                            "idProp", idMapping.getProp()));
            getNamedParameterJdbcTemplate().batchUpdate(sql, sources);
            return chunk.size();
        });
    }

    @Override
    public int batchUpdateByIdWithOptiVersion(List<String> includePropNames, List<ENTITY> list, int batchSize) {
        String setters = mappings.getAllMapping().stream()
                .filter(mapping -> !mapping.getColumn().equals(idMapping.getColumn()))
                .filter(mapping -> !mapping.getColumn().equals(versionMapping.getColumn()))
                .map(it -> it.getColumn() + "=:" + it.getProp())
                .collect(Collectors.joining(","));
        return Sqls.doInBatch(list, batchSize, chunk -> {
            var sources = chunk.stream().map(BeanPropertySqlParameterSource::new)
                    .toArray(size -> new BeanPropertySqlParameterSource[size]);
            var sql = Strs.format("update {table} set {setters},{versionColumn}={versionColumn}+1 where {idColumn}=:{idProp} and {versionColumn}=:{versionProp}",
                    Map.of("table", tableName,
                            "setters", setters,
                            "versionColumn", versionMapping.getColumn(),
                            "versionProp", versionMapping.getProp(),
                            "idColumn", idMapping.getColumn(),
                            "idProp", idMapping.getProp()));
            getNamedParameterJdbcTemplate().batchUpdate(sql, sources);
            return chunk.size();
        });
    }

    @Override
    public <R> PageRes<R> queryByPage(String sql, SqlParameterSource paramSource, Class<R> clazz, PageParam pageParam) {
        sql = sql.trim();
        if (sql.endsWith(";")) {
            sql = sql.substring(0, sql.length() - 1);
        }
        PageRes<R> pageRes = new PageRes<>();
        var countSql = Strs.format("select count(*) from ({origSql}) _txpqmsf", Map.of("origSql", sql));
        var total = getNamedParameterJdbcTemplate().queryForObject(countSql, paramSource, Integer.class);
        pageRes.setTotal(total);
        if (total > pageParam.offset()) {
            var rowsSql = Strs.format("{origSql} limit {limit} offset {offset}",
                    Map.of("origSql", sql,
                            "limit", pageParam.getSize(),
                            "offset", pageParam.offset()));
            var rows = getNamedParameterJdbcTemplate().query(rowsSql, paramSource, new BeanPropertyRowMapper<>(clazz));
            pageRes.setRows(rows);
        } else {
            pageRes.setRows(List.of());
        }
        return pageRes;
    }

    @Override
    public int deleteById(Long id) {
        var params = Map.of("id", id);
        var allColumnsString = String.join(",", mappings.getColumnName2Mapping().keySet());
        if (StringUtils.hasText(hisTableName)) {
            var bakSql = String.format("insert into {hisTable}({allColumns}) select {allColumns} from {table} where {idColumn}=:id",
                    Map.of("hisTable", hisTableName,
                            "allColumns", allColumnsString,
                            "table", tableName,
                            "idColumn", idMapping.getColumn()));
            getNamedParameterJdbcTemplate().update(bakSql, params);
        }
        var sql = Strs.format("delete from {table} where {idColumn}=:id",
                Map.of("table", tableName,
                        "idColumn", idMapping.getColumn()));
        return getNamedParameterJdbcTemplate().update(sql, params);
    }

    @Override
    public int batchDeleteById(List<Long> ids, int batchSize) {
        var allColumnsString = String.join(",", mappings.getColumnName2Mapping().keySet());
        return Sqls.doInBatch(ids, batchSize, chunk -> {
            var params = Map.of("ids", chunk);
            if (StringUtils.hasText(hisTableName)) {
                var bakSql = String.format("insert into {hisTable}({allColumns}) select {allColumns} from {table} where {idColumn} in(:ids)",
                        Map.of("hisTable", hisTableName,
                                "allColumns", allColumnsString,
                                "table", tableName,
                                "idColumn", idMapping.getColumn()));
                getNamedParameterJdbcTemplate().update(bakSql, params);
            }
            var sql = Strs.format("delete from {table} where {idColumn} in(:ids)",
                    Map.of("table", tableName,
                            "idColumn", idMapping.getColumn()));
            return getNamedParameterJdbcTemplate().update(sql, params);
        });
    }
}