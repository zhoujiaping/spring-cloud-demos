package org.example.common.jdbc;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.util.ReflectionUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Mappings {
    private List<Mapping> allMapping = new ArrayList<>();
    private Map<String, Mapping> columnName2Mapping = new HashMap<>();
    private Map<String, Mapping> propName2Mapping = new HashMap<>();

    public static Mappings fromEntityClass(Class<?> entityClass) {
        Mappings mappings = new Mappings();
        ReflectionUtils.doWithFields(entityClass, field -> {
                    Annotation[] annos = field.getAnnotations();
                    Mapping mapping = null;
                    for (Annotation anno : annos) {
                        if (anno.annotationType().equals(Id.class)) {
                            mapping = new IdMapping();
                        } else if (anno.annotationType().equals(Version.class)) {
                            mapping = new VersionMapping();
                        } else if (anno.annotationType().equals(BigField.class)) {
                            mapping = new BigFieldMapping();
                        }
                    }
                    if (mapping == null) {
                        mapping = new Mapping();
                    }
                    mapping.setColumn(Sqls.toColumnName(field.getName()));
                    mapping.setProp(field.getName());
                    mapping.setField(field);
                    mappings.addMapping(mapping);
                }, field ->
                        !Modifier.isStatic(field.getModifiers()) &&
                                !Modifier.isFinal(field.getModifiers()) &&
                                !Modifier.isTransient(field.getModifiers())
        );
        return mappings;
    }

    public List<Mapping> getAllMapping() {
        return allMapping;
    }

    public Map<String, Mapping> getColumnName2Mapping() {
        return columnName2Mapping;
    }

    public Map<String, Mapping> getPropName2Mapping() {
        return propName2Mapping;
    }

    public Mappings addMapping(Mapping mapping) {
        allMapping.add(mapping);
        columnName2Mapping.put(mapping.getColumn(), mapping);
        propName2Mapping.put(mapping.getProp(), mapping);
        return this;
    }
}
