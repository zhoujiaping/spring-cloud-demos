package org.example.common.jdbc;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.lang.reflect.Field;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Mapping {
    Field field;
    String column;
    String prop;
}
