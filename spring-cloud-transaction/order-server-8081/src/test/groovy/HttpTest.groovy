import lombok.Cleanup
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import org.junit.jupiter.api.Test

class HttpTest {
    OkHttpClient client = new OkHttpClient();

    @Test
    void testCreateOrder() {
        String url = "http://localhost:8081/orders"
        def mediaType = MediaType.parse("application/json;charset=UTF-8")
        def body = RequestBody.create("""{"orderNo":"Order000001"}""", mediaType)

        Request request = new Request.Builder()
                .post(body)
                .url(url)
                .build()

        @Cleanup Response response = client.newCall(request).execute()
        def content = response.body().string()
        println content
    }
}
