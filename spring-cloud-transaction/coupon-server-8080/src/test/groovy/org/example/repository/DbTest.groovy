package org.example.repository

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import lombok.Cleanup
import org.junit.jupiter.api.Test

class DbTest {
    @Test
    void testConnection() {
        def config = new HikariConfig(
                maximumPoolSize: 100,
                dataSourceClassName: 'com.mysql.cj.jdbc.MysqlDataSource',
                dataSourceProperties: [
                        serverName:'localhost',
                        port:3306,
                        databaseName:'test',
                        user:'root',
                        password:'123456'
                ] as Properties
        )
        @Cleanup def ds = new HikariDataSource(config)
        @Cleanup def ps = ds.getConnection().prepareStatement('show databases;')
        def rs = ps.executeQuery()
        rs.each{
            println it
        }
    }
}
