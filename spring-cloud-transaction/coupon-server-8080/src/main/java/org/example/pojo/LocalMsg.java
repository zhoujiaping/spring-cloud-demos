package org.example.pojo;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LocalMsg {
    Long id;
    String bizType;
    String content;
    String status;
    Long createdAt;
    Long lastUpdatedAt;
    Integer version;
}
