package org.example.pojo;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Order {
    Long id;
    Long userId;
    String orderNo;
    Long goodsId;
    Long deliveryAddrId;
    BigDecimal price;
    String status;
    Long createdAt;
    Long lastUpdatedAt;
    Integer version;
}
