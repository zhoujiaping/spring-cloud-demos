package org.example.pojo;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Coupon {
    Long id;
    Long userId;
    Integer amount;
    Long createdAt;
    Long lastUpdatedAt;
    Integer version;
}
