package org.example.controller;

import org.example.pojo.Coupon;
import org.example.pojo.Order;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/coupons")
public class CouponController {
    @PostMapping
    public Mono<?> createOrder(@RequestBody Order order){
        return Mono.just(Coupon.builder().amount(1000).build());
    }
}
