-- 本地消息表
create table t_local_msg(
    id bigint primary key auto_increment comment '主键',
    biz_type varchar(64) not null comment '业务类型',
    content text not null comment '消息内容',
    status varchar(36) not null default 'PREPARED' comment '消息状态。PREPARED:已准备，COMMITTED：已消费',
    created_at timestamp not null default current_timestamp comment '创建时间',
    last_updated_at timestamp not null default current_timestamp comment '最近更新时间',
    version int not null default 1 comment '乐观锁版本号'
);
-- 订单表（为了简化问题，假设一个订单只能有一个商品，假设不支持货到付款，）
create table t_order(
    id bigint primary key auto_increment comment '主键',
    user_id bigint not null comment '用户id',
    order_no varchar(64) not null comment '订单号',
    goods_id bigint not null comment '商品id',
    delivery_addr_id bigint not null comment '收货地址id',
    price decimal(10,2) not null comment '订单价格',
    status varchar(36) not null default 'PREPARED' comment '订单状态。PLACED:已下单，FINISHED:已完成，CANCELLED:已取消，CHARGEBACK:已退单',
    created_at timestamp not null default current_timestamp comment '创建时间',
    last_updated_at timestamp not null default current_timestamp comment '最近更新时间',
    version int not null default 1 comment '乐观锁版本号'
);
-- 积分表
create table t_coupon(
    id bigint primary key auto_increment comment '主键',
    user_id bigint not null comment '用户id',
    amount int not null default 0 comment '积分数量',
    created_at timestamp not null default current_timestamp comment '创建时间',
    last_updated_at timestamp not null default current_timestamp comment '最近更新时间',
    version int not null default 1 comment '乐观锁版本号'
);